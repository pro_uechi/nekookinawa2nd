﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RoundAnimation : MonoBehaviour
{
    private RectTransform rectTransform;
    private Sequence seq;
    private Vector3 initPostion;
    public static float AnimTime = 0.2f;
    public static float waitTime = 2.0f;

	/// <summary>
	/// round xx
	/// </summary>
	[SerializeField]
	private GameObject RoundObj = null;


	[SerializeField]
    private float secondEndPosY = 0;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        initPostion = transform.position;
        StartCoroutine(Setseq());
    }

    private void OnEnable()
    {
        ResetTransform();
        StartCoroutine(Setseq());
    }

    /// <summary>
    /// 2、3Roundでラウンドのアニメーションをするため
    /// </summary>
    private void ResetTransform()
    {
        transform.position = initPostion;
    }

    IEnumerator Setseq()
    {
        Sequence seq = DOTween.Sequence();
        rectTransform = this.GetComponent<RectTransform>();

        //seq.Append(rectTransform.DOLocalMoveY(0.0f, AnimTime));
        yield return new WaitForSeconds(waitTime);
        seq.Append(rectTransform.DOLocalMoveY(secondEndPosY, AnimTime));
    }
}
