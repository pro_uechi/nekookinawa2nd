﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ReadyGoAnimation : MonoBehaviour
{
    private RectTransform rectTransform;
    private Sequence seq;
    private Vector3 initPostion;
    public static float AnimTime = 0.2f;
    public static float waitTime = 1.0f;

	/// <summary>
	/// ReadyのUI
	/// </summary>
	[SerializeField]
	private GameObject ReadyObj = null;
	/// <summary>
	/// FightのUI
	/// </summary>
	[SerializeField]
	private GameObject FightObj = null;

	[SerializeField]
    private float secondEndPosX = 0;

    //private void Awake()
    //{

    //}

    private void OnEnable()
    {
        ResetTransform();
        StartCoroutine(Setseq());
    }

    /// <summary>
    /// Ready Fight アニメーション
    /// </summary>
    private void ResetTransform()
    {
		ReadyObj.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
		FightObj.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

		//transform.position = initPostion;
		//rectTransform = this.GetComponent<RectTransform>();
		ReadyObj.transform.DOScale(new Vector3(0,0,0), 0.0f);
		FightObj.transform.DOScale(new Vector3(0, 0, 0), 0.0f);

	}

    IEnumerator Setseq()
    {
        Sequence seq = DOTween.Sequence();

		seq.Append(ReadyObj.transform.DOScale(new Vector3(1, 1, 1), 0.3f));
        yield return new WaitForSeconds(waitTime);

		seq.Join(ReadyObj.transform.DOScale(new Vector3(0, 0, 0), 0.0f));
		seq.Join(FightObj.transform.DOScale(new Vector3(1, 1, 1), 0.4f));
		yield return new WaitForSeconds(waitTime);
		//seq.Append(FightObj.transform.DOScale(new Vector3(0, 0, 0), 0.1f));
		seq.Join(FightObj.transform.DOLocalMoveY(secondEndPosX, 0.2f));
		//yield return null;
	}
}
