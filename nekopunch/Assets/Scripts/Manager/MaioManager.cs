﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MaioManager : SingletonMonoBehaviour<MaioManager>
{
    /// <summary>
    /// 初期化が完了しているかどうか
    /// </summary>
    public static bool initialized;

    /// <summary>
    /// 動画再生タイプ
    /// </summary>
    private AdType adType;

    /// <summary>
    /// スキップしたかどうか
    /// </summary>
    private bool skipFlag = false;

    /// <summary>
    /// ポップアップを表示するかどうか
    /// </summary>
    public static bool popupChecked = true;

    /// <summary>
    /// 連続動画再生防止フラグ
    /// </summary>
    public static bool showed = false;


    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
        Debug.Log("Maio初期化");

        initialized = false;

        // 広告の配信テスト設定を行います。アプリをリリースする際にはコメントアウトしてください。
        Maio.SetAdTestMode(true);
        // maio SDKの初期化を開始します。
        // MaioMediaId は、Maio管理画面より割り当てられたメディアIDに差し替えてください。
        Maio.Start(GameData.MaioMediaId);

        // Maioのイベント登録
        // ここで登録するイベントは動画広告を表示する全シーン共通
        // 共通でないイベントは動画広告を表示する任意の場所で登録、破棄する
        Maio.OnInitialized += HandleOnInitialized;
        Maio.OnChangedCanShow += HandleOnChangedCanShow;
        Maio.OnStartAd += HandleOnStartAd;
        Maio.OnClickedAd += HandleOnClickedAd;
        Maio.OnClosedAd += HandleOnClosedAd;
        Maio.OnFailed += HandleOnFailed;
        Maio.OnFinishedAd += HandleOnFinishedAd;
    }

    /// <summary>
    /// 広告表示できるかどうか
    /// </summary>
    /// <returns></returns>
    public bool CanShowAd()
    {
#if UNITY_IOS
        return Maio.CanShow() && MaioManager.initialized && showed == false;
        
#elif UNITY_ANDROID
        return Maio.CanShow(GameData.MaioZoneId) && MaioManager.initialized && showed == false;
#else
#endif
    }

    /// <summary>
    /// 広告表示
    /// </summary>
    public void ShowAd(AdType type)
    {
        Debug.Log("ShowAd()");
        adType = type;
        GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));

        switch (type)
        {
            case AdType.Home:
#if UNITY_IOS
                msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("動画を見て\nコインをゲットしますか？", delegate {Maio.Show(); }, null);
#elif UNITY_ANDROID
                msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("動画を見て\nプレゼントをゲットしますか？", delegate { Maio.Show(GameData.MaioZoneId); }, null);
#else
#endif
                break;
            case AdType.BattleContinue:
#if UNITY_IOS
                msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("動画を見て\nコインをゲットしますか？", delegate {Maio.Show(); }, null);
#elif UNITY_ANDROID
                msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("動画を見て\nコンティニューしますか？", delegate { Maio.Show(GameData.MaioZoneId); }, null);
#else
#endif
                break;
            case AdType.LoginBonus:
                Destroy(msgBox);
#if UNITY_IOS
                Maio.Show();
#elif UNITY_ANDROID
                Maio.Show(GameData.MaioZoneId);
#else
#endif
                break;
        }
    }

    /// <summary>
    /// 全てのゾーン（広告スポット）の広告表示準備が完了した際に呼ばれます。
    /// </summary>
    private void HandleOnInitialized()
    {
        Debug.Log("Maio.OnInitialized()");
        initialized = true;
    }

    /// <summary>
    /// 広告の配信可能状態が変更された際に呼ばれます。
    /// </summary>
    /// <param name="zoneId"></param>
    /// <param name="newValue"></param>
    private void HandleOnChangedCanShow(string zoneId, bool newValue)
    {
        Debug.LogFormat("Maio.OnChangedCanShow('{0}', {1})", zoneId, newValue);
    }

    /// <summary>
    /// 動画が再生される直前に呼ばれます。  
    /// 最初の再生開始の直前にのみ呼びだされ、リプレイ再生の直前には呼ばれません。
    /// </summary>
    /// <param name="zoneId"></param>
    private void HandleOnStartAd(string zoneId)
    {
        Debug.LogFormat("Maio.OnStartAd('{0}')", zoneId);
    }

    /// <summary>
    /// 広告がクリックされ、AppStoreや外部リンクへ遷移した際に呼ばれます。
    /// </summary>
    /// <param name="zoneId"></param>
    private void HandleOnClickedAd(string zoneId)
    {
        Debug.LogFormat("Maio.OnClickedAd('{0}')", zoneId);
    }

    /// <summary>
    /// 広告が閉じられた際に呼び出されます。
    /// </summary>
    /// <param name="zoneId"></param>
    private void HandleOnClosedAd(string zoneId)
    {
        Debug.LogFormat("Maio.OnClosedAd('{0}')", zoneId);
    }

    /// <summary>
    /// エラーが発生した際に呼び出されます。
    /// </summary>
    /// <param name="zoneId"></param>
    /// <param name="reason"></param>
    private void HandleOnFailed(string zoneId, Maio.FailReason reason)
    {
        Debug.LogFormat("Maio.OnFailed({0}, {1})", zoneId == null ? "null" : "'" + zoneId + "'", reason);
    }

    /// <summary>
    /// 広告の視聴が終了した際に呼ばれます。
    /// </summary>
    /// <param name="zoneId"></param>
    /// <param name="playtime"></param>
    /// <param name="skipped"></param>
    /// <param name="rewardParam"></param>
    private void HandleOnFinishedAd(string zoneId, int playtime, bool skipped, string rewardParam)
    {
        Debug.LogFormat("Maio.OnFinishedAd('{0}', {1}, {2}, '{3}' )", zoneId, playtime, skipped, rewardParam);

        showed = true;

        // スキップ状態を設定
        skipFlag = skipped;

        // スキップしていなければ報酬を与える
        if (!skipped)
        {
            // 報酬を分岐させる
            switch (adType)
            {
                case AdType.Home:
                    break;

                case AdType.BattleContinue:
                    SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
                    break;
            }
        }
        else
        {
            switch (adType)
            {
                case AdType.Home:
                    break;

                case AdType.BattleContinue:
                    GameData.UserData.battleCount = 0;
                    break;
            }
        }

        // ポップアップチェックの状態を設定
        popupChecked = false;
    }

    /// <summary>
    /// ポップアップチェック
    /// </summary>
    public void CheckPopUp()
    {
        if (!popupChecked)
        {
            // ポップアップチェックの状態を設定
            popupChecked = true;

            if (!skipFlag)
            {
                // 報酬を分岐させる
                switch (adType)
                {
                    case AdType.Home:
                        GameObject.Find("RewardsSystem").GetComponent<RewardsSystem>().StartRoulette();
                        break;

                    case AdType.BattleContinue:
                        break;


                }
            }
            else
            {
                switch (adType)
                {
                    case AdType.Home:
                        GameObject msgBox2 = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                        msgBox2.GetComponent<MessageBoxManager>().Initialize_OK("最後まで見てね", null);
                        break;

                    case AdType.BattleContinue:
                        GameObject msgBox3 = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                        msgBox3.GetComponent<MessageBoxManager>().Initialize_OK("次は最後まで見てね", delegate { SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed); });
                        break;


                }
            }
        }
    }
}
