﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAdsManager : SingletonMonoBehaviour<UnityAdsManager>
{
    /// <summary>
    /// 動画再生タイプ
    /// </summary>
    private AdType adType;

    /// <summary>
    /// 連続動画再生防止フラグ
    /// </summary>
    public static bool showed = false;


    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
        Debug.Log("UnityAds初期化");

#if UNITY_IOS
        Advertisement.Initialize ("3115669");
#elif UNITY_ANDROID
        Advertisement.Initialize("3115668");
#endif
    }

    /// <summary>
    /// 動画広告表示
    /// </summary>
    /// <param name="type"></param>
    public void ShowAd(AdType type)
    {
        if (Advertisement.IsReady())
        {
            Debug.Log("ShowAd()");
            adType = type;
            GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
            var options = new ShowOptions { resultCallback = HandleShowResult };

            switch (type)
            {
                case AdType.Home:
                    msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("動画を見て\nプレゼントをゲットしますか？", delegate { Advertisement.Show(options); }, null);
                    break;
                case AdType.BattleContinue:
                    //msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("動画を見て\nコンティニューしますか？", delegate { Advertisement.Show(options); }, 
                    //    delegate 
                    //    {
                    //        var gm = GameObject.Find("GameManager").GetComponent<GameManager>();
                    //        gm.ResetPlayers();
                    //        gm.InitializeData();
                    //        SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed); 
                    //    });
                    Destroy(msgBox);
                    Advertisement.Show(options);
                    break;
                case AdType.LoginBonus:
                    Destroy(msgBox);
                    Advertisement.Show(options);
                    break;
                // 次のバトルに移動する際に出る広告
                case AdType.NextBattle:
                    Destroy(msgBox);
                    Advertisement.Show(options);
                    break;
                case AdType.WaitTimeHome:
                    msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("広告を見て\n時間を進めますか？", delegate { GameData.IsHomeAbs = true;  Advertisement.Show(options); ItemManager.Instance.LeaveHomeInitialize(); }, null);
                    break;
            }
        }
    }

    /// <summary>
    /// 広告表示できるかどうか
    /// </summary>
    /// <returns></returns>
    public bool CanShowAd()
    {
        return Advertisement.IsReady() && showed == false;
    }

    /// <summary>
    /// 動画広告結果
    /// </summary>
    /// <param name="result"></param>
    private void HandleShowResult(ShowResult result)
    {
        showed = true;

        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                // 報酬を分岐させる
                switch (adType)
                {
                    case AdType.Home:
                        GameObject.Find("RewardsSystem").GetComponent<RewardsSystem>().StartRoulette();
                        break;

                    case AdType.BattleContinue:
                    case AdType.NextBattle:
                        SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
                        break;

                    //case AdType.LoginBonus:
                    //    GameObject.Find("LoginBonusSystem").GetComponent<LoginBonusSystem>().UpDateData();
                    //    GameObject.Find("LoginBonusSystem").GetComponent<LoginBonusSystem>().ConfirmLoginBonusItem();
                    //    GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                    //    msgBox.GetComponent<MessageBoxManager>().Initialize_OK("２倍でもらえました。", null);
                    //    break;
                    case AdType.WaitTimeHome:
                        ItemManager.Instance.StartAnimation(120);
                        GameData.IsHomeAbs = false;
                        break;
                  
                }
                break;

            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                switch (adType)
                {
                    case AdType.Home:
                        GameObject msgBox2 = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                        msgBox2.GetComponent<MessageBoxManager>().Initialize_OK("最後まで見ないと\nプレゼントがもらえないよ。", null);
                        break;

                    case AdType.BattleContinue:
                        GameData.UserData.battleCount = 0;
                        GameObject msgBox3 = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                        msgBox3.GetComponent<MessageBoxManager>().Initialize_OK("次は最後まで見てね。", delegate { SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed); });
                        break;


                    case AdType.NextBattle:
                        GameObject msgBox5 = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
                        msgBox5.GetComponent<MessageBoxManager>().Initialize_OK("次は最後まで見てね。", delegate { SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed); });
                        break;
                    case AdType.WaitTimeHome:
                        ItemManager.Instance.StartAnimation(1);
                        GameData.IsHomeAbs = false;
                        break;
                }
                break;

            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
