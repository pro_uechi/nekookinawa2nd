﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SettingManager : SingletonMonoBehaviour<SettingManager>
{
    /// <summary>
    /// BGMボタン
    /// </summary>
    [SerializeField]
    private GameObject BGMVolumeButton;

    /// <summary>
    /// SEボタン
    /// </summary>
    [SerializeField]
    private GameObject SEVolumeButton;

    [SerializeField]
    private Sprite onSprite = null;
    [SerializeField]
    private Sprite offSprite = null;
    [SerializeField]
    private Sprite onSESprite = null;
    [SerializeField]
    private Sprite offSESprite = null;

    /// <summary>
    /// キャラクター一覧のオブジェクト
    /// </summary>
    [SerializeField]
    private GameObject characterList;
    private GameObject CharacterListContent;

    /// <summary>
    /// クレジット一覧
    /// </summary>
    [SerializeField]
    private GameObject creditsList = null;

    /// <summary>
    /// 各メールのタイトル名
    /// </summary>
    private const string MAILTITLE_FAILER = " 不具合";
    private const string MAILTITLE_REQUEST = " 要望";
    private const string MAILTITLE_ETC = " その他";

    private const string ANNOUNCE_FAILER = " 不具合の内容をご記入ください";
    private const string ANNOUNCE_REQUEST = " ご意見・ご要望の内容をご記入ください";
    private const string ANNOUNCE_ETC = " その他の内容をご記入ください";

    /// <summary>
    /// メールのポップアップオブジェクト
    /// </summary>
    public GameObject MailPopUp;

    /// <summary>
    /// ユーザーID
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI userIDText = null;


    /// <summary>
    /// Webビュー
    /// </summary>
    private WebViewObject m_webViewObject;

    //private const string URL = "https://policies.google.com/?hl=ja";
    private const string URL = "http://prolead.co.jp/transaction/privacy.html";
    private int MARGIN_X = Screen.width / 9;
    private int MARGIN_Y = Screen.height / 5;
    private int OUT_MARGIN_AJUST = 2000;

    /// <summary>
    /// 引き継ぎIDInput
    /// </summary>
    [SerializeField]
    private GameObject IDInput = null;


    private void Start()
    {
        if(SaveData.GetInt(SaveKey.BGMSetting) == GameData.SETTING_SOUND_NONE)
        {
            SaveData.SetInt(SaveKey.BGMSetting, GameData.SETTING_SOUND_ON);
            SaveData.Save();
        }

        if (SaveData.GetInt(SaveKey.SESetting) == GameData.SETTING_SOUND_NONE)
        {
            SaveData.SetInt(SaveKey.SESetting, GameData.SETTING_SOUND_ON);
            SaveData.Save();
        }

        // 初期ボタン設定
        if (AudioManager.Instance.GetBGMFlag() == GameData.SETTING_SOUND_ON)
        {
            BGMVolumeButton.GetComponent<Image>().sprite = onSprite;
        }
        else
        {
            BGMVolumeButton.GetComponent<Image>().sprite = offSprite;
        }

        if (AudioManager.Instance.GetSEFlag() == GameData.SETTING_SOUND_ON)
        {
            SEVolumeButton.GetComponent<Image>().sprite = onSESprite;
        }
        else
        {
            SEVolumeButton.GetComponent<Image>().sprite = offSESprite;
        }

        InitButton();
        SetUserInfo();

        ////Webビューで表示するページを先読み
        //LoadWebView(URL);
    }


    private void InitButton()
    {
        MailPopUp.SetActive(false);

        //CharacterListContent = GameObject.Find("Canvas_UI/SafeArea/CharacterList/Scroll View/Viewport/Content").gameObject;
        //characterList.SetActive(false);
    }

    /// <summary>
    /// IDと名前を設定する
    /// </summary>
    private void SetUserInfo()
    {
        userIDText.text = "XZ" + GameData.UserData.UserId + "HP";
        //userNameText.text = GameData.UserData.UserName;
    }

    private void Update()
    {
        ////シーン読み込み中あれば、WebViewを消す(バトルシーンに移動時のみに必要)
        //m_webViewObject.SetVisibility(!SceneFadeManager.SceneLoadFlg);
    }

    /// <summary>
    /// BGMのボリュームON/OFF
    /// </summary>
    public void BGMVolumeButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");

        int volumeData = SaveData.GetInt(SaveKey.BGMSetting);

        /// <summary>
        /// BGMのボリュームがONならOFFに、
        /// BGMのボリュームがOFFならONにする
        /// </summary>
        switch(volumeData)
        {
            case GameData.SETTING_SOUND_NONE:
            case GameData.SETTING_SOUND_OFF:
                SaveData.SetInt(SaveKey.BGMSetting, GameData.SETTING_SOUND_ON);
                SaveData.Save();
                BGMVolumeButton.GetComponent<Image>().sprite = onSprite;
                AudioManager.Instance.SetBGMFlag(GameData.SETTING_SOUND_ON);
                AudioManager.Instance.PlayBGM(AudioManager.Instance.currentBGMKey);
                break;
            case GameData.SETTING_SOUND_ON:
                SaveData.SetInt(SaveKey.BGMSetting, GameData.SETTING_SOUND_OFF);
                SaveData.Save();
                BGMVolumeButton.GetComponent<Image>().sprite = offSprite;
                AudioManager.Instance.SetBGMFlag(GameData.SETTING_SOUND_OFF);
                break;
        }

    }

    /// <summary>
    /// SEのボリュームON/OFF
    /// </summary>
    public void SEVolumeButtonOnClick()
    {
        //AudioManager.Instance.PlaySE("OkSe");
        int volumeData = SaveData.GetInt(SaveKey.SESetting);

        /// <summary>
        /// BGMのボリュームがONならOFFに、
        /// BGMのボリュームがOFFならONにする
        /// </summary>
        switch (volumeData)
        {
            case GameData.SETTING_SOUND_NONE:
            case GameData.SETTING_SOUND_OFF:
                SaveData.SetInt(SaveKey.SESetting, GameData.SETTING_SOUND_ON);
                SaveData.Save();
                SEVolumeButton.GetComponent<Image>().sprite = onSESprite;
                AudioManager.Instance.SetSEFlag(GameData.SETTING_SOUND_ON);
                break;
            case GameData.SETTING_SOUND_ON:
                SaveData.SetInt(SaveKey.SESetting, GameData.SETTING_SOUND_OFF);
                SaveData.Save();
                SEVolumeButton.GetComponent<Image>().sprite = offSESprite;
                AudioManager.Instance.SetSEFlag(GameData.SETTING_SOUND_OFF);
                break;
        }

    }


    /// <summary>
    /// 利用規約ボタン押下時
    /// </summary>
    public void LegalButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        Application.OpenURL(GameData.REGAL_URL);
    }

    /// <summary>
    /// レビューボタン押下時
    /// </summary>
    public void ReviewButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
#if UNITY_ANDROID
        Application.OpenURL(GameData.REVIEW_URL_ANDROID);
#else
        Application.OpenURL(GameData.REVIEW_URL_IPHONE);
#endif
    }

    /// <summary>
    /// 不具合ボタン押下時
    /// </summary>
    public void SupportFaliureButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        OpenMailer.OpenSupportMail(MAILTITLE_FAILER, ANNOUNCE_FAILER);
    }

    /// <summary>
    /// 要望ボタン押下時
    /// </summary>
    public void SupportRequestButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        OpenMailer.OpenSupportMail(MAILTITLE_REQUEST, ANNOUNCE_REQUEST);
    }

    /// <summary>
    /// その他ボタン押下時
    /// </summary>
    public void SupportEtcButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        OpenMailer.OpenSupportMail(MAILTITLE_ETC, ANNOUNCE_ETC);
    }

    /// <summary>
    /// メールのポップアップを閉じる
    /// </summary>
    public void SupportPopUpEnable(bool flg)
    {
        AudioManager.Instance.PlaySE("OkSe");
        MailPopUp.SetActive(flg);
    }

    /// <summary>
    /// 名前変更ボタン押下時
    /// </summary>
    public void ReNameButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        GameObject parent = GameObject.Find("Canvas_UI/SafeArea");
        // UserDataManager.Instance.SetUserName(OkAction, parent);
    }


    /// <summary>
    /// クレジットボタン押下時
    /// </summary>
    public void CreditsButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        creditsList.SetActive(true);
       // closeSettingButton.SetActive(false);
    }

    /// <summary>
    /// クレジットを閉じるボタン押下時
    /// </summary>
    public void CloseCreditsButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        creditsList.SetActive(false);
      //  closeSettingButton.SetActive(true);
    }

    /// <summary>
    /// プライポリシー押下時
    /// </summary>
    public void PrivacyPolicyOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        Application.OpenURL(URL);
        //#if UNITY_EDITOR_WIN
        //        // MacでないとUnity上でデバックできないため
        //        Application.OpenURL(URL);

        //#else
        //        SetWebViewDisplay();
        //#endif
    }

    /// <summary>
    /// 特定商取引法に基づく表示ボタン押下時
    /// </summary>
    public void TradeLawButton_OnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        Application.OpenURL("http://prolead.co.jp/transaction/agreement.html");
    }

    /// <summary>
    /// データ引き継ぎボタン押下時
    /// </summary>
    public void DataTransferButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        IDInput.SetActive(true);
    }

    /// <summary>
    /// OKボタン押下時
    /// </summary>
    public void IDInput_OKButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        StartCoroutine(DataTransfer());
    }

    /// <summary>
    /// キャンセルボタン押下時
    /// </summary>
    public void IDInput_CancelButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        IDInput.SetActive(false);
    }

    /// <summary>
    /// データ引き継ぎ
    /// </summary>
    /// <returns></returns>
    private IEnumerator DataTransfer()
    {
        // 入力されたIDがデータベースに存在するかチェック
        string id = IDInput.transform.Find("Base/InputField").GetComponent<InputField>().text;
        yield return StartCoroutine(DBAccessManager.Instance.GetTransferData(id));

        // IDの有無によって処理を分岐
        if (DBAccessManager.canTransfer)
        {
            // ローカルのユーザーIDを書き換えてセーブ
            SaveData.SetInt(SaveKey.UserId, GameData.ConvertStringToUserID(id));
            SaveData.Save();

            // ポップアップ
            RebootMessage();
        }
        else
        {
            GameObject msgBox = Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
            msgBox.GetComponent<MessageBoxManager>().Initialize_OK("コードが違います。", null);
        }
    }

    /// <summary>
    /// 再起動メッセージ
    /// </summary>
    private void RebootMessage()
    {
        GameObject msgBox = Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        msgBox.GetComponent<MessageBoxManager>().Initialize_OK("引き継ぎ完了!\nアプリを再起動してください。", RebootMessage);
    }

    /// <summary>
    /// 引継ぎID発行ボタン押下時
    /// </summary>
    public void IssueDataTransferIDButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        StartCoroutine(IssueDataTransferID());
    }

    /// <summary>
    /// 引継ぎID発行
    /// </summary>
    /// <returns></returns>
    private IEnumerator IssueDataTransferID()
    {
        string id = GameData.ConvertUserIDToString(GameData.UserData.UserId);
        yield return StartCoroutine(DBAccessManager.Instance.InsertTransferData(id));

        GameObject msgBox = Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        //msgBox.GetComponent<MessageBoxManager>().Initialize_OK("引継ぎコード\n<color=#FF0000>" + id + "</color>\n大切に保管してください。\n有効期間は14日です", null);
        msgBox.GetComponent<MessageBoxManager>().Initialize_OK("引継ぎコード\n<color=#FF0000>" + id, null);
    }

    /// <summary>
    /// タイトルに戻るボタン押下時
    /// </summary>
    public void ReturnTitleOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        SceneFadeManager.Instance.Load(GameData.Scene_Splash, GameData.FadeSpeed);
    }

    /// <summary>
    /// WebViewを画面上に表示
    /// </summary>
    private void SetWebViewDisplay()
    {
        // WebView のマージンを設定します
        m_webViewObject.SetMargins(MARGIN_X, MARGIN_Y, MARGIN_X, MARGIN_Y);
    }

    /// <summary>
    /// Webビューで使うオブジェクトの生成と読み込み（画面外に表示）
    /// </summary>
    private void LoadWebView(string url)
    {
        Destroy(m_webViewObject);

        var go = new GameObject("WebViewObject");

        m_webViewObject = go.AddComponent<WebViewObject>();

        // WebView を初期化します
        m_webViewObject.Init
        (
            cb: msg => Debug.LogFormat("HTML からメッセージを取得しました: {0}", msg),
            err: msg => Debug.LogFormat("エラーが発生しました: {0}", msg),
            started: msg => Debug.LogFormat("ページの読み込みを開始しました: {0}", msg),
            ld: msg => Debug.LogFormat("ページの読み込みが完了しました: {0}", msg),
            enableWKWebView: true
        );

        // WebView のマージンを設定します(画面外へ表示しておく)
        m_webViewObject.SetMargins(MARGIN_X + OUT_MARGIN_AJUST, MARGIN_Y, MARGIN_X - OUT_MARGIN_AJUST, MARGIN_Y);

        m_webViewObject.SetVisibility(true);

        // 指定した URL を読み込みます
        m_webViewObject.LoadURL(url);
    }

    /// <summary>
    /// 閉じるボタン押下時
    /// </summary>
    public void CloseWebView()
    {
        // WebView を非表示状態にします
        //m_webViewObject.SetVisibility(false);

        //LoadWebView(URL);//動画など音が出る場合はオブジェクトは破壊して再読み込み
        // 指定した URL を読み込みます
        // WebView のマージンを設定します
        m_webViewObject.SetMargins(MARGIN_X + OUT_MARGIN_AJUST, MARGIN_Y, MARGIN_X - OUT_MARGIN_AJUST, MARGIN_Y);
        //m_webViewObject.LoadURL(URL);


    }

    /// <summary>
    /// 閉じるボタン押下時
    /// </summary>
    public void CloseSettingButtonOnClick()
    {
        SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
    }


    /// <summary>
    /// 設定ボタン押下時
    /// </summary>
    public void SettingButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        if (this.gameObject.activeSelf)
        {
            SaveData.Save();
            SettingOff();
            //UserDataManager.Instance.DestroyInputObj();
        }
        else
        {
            SettingOn();
        }
    }

    /// <summary>
    /// 設定画面表示
    /// </summary>
    public void SettingOn()
    {
        this.gameObject.SetActive(true);
    }

    /// <summary>
    /// 設定画面非表示
    /// </summary>
    public void SettingOff()
    {
        this.gameObject.SetActive(false);
    }
}
