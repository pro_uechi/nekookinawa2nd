﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// サウンド関連
/// </summary>
public class AudioManager : SingletonMonoBehaviour<AudioManager>
{

    /// <summary>
    /// BGMキー名
    /// </summary>
    public string[] bgmKey;

    /// <summary>
    /// 現在のBGMキー
    /// </summary>
    public string currentBGMKey;

    /// <summary>
    /// SEキー名
    /// </summary>
    public string[] seKey;

    /// <summary>
    /// BGM音源
    /// </summary>
    public AudioClip[] bgmFiles;

    /// <summary>
    /// SE音源
    /// </summary>
    public AudioClip[] seFiles;

    /// <summary>
    /// BGM再生オブジェクト
    /// </summary>
    public AudioSource bgmSource;

    /// <summary>
    /// SE再生オブジェクト
    /// </summary>
    public AudioSource seSource;

    /// <summary>
    /// BGMが再生できるかどうか
    /// </summary>
    public int playBGMFlag;

    /// <summary>
    /// SEが再生できるかどうか
    /// </summary>
    public int playSEFlag;


    /// <summary>
    /// BGM一覧
    /// </summary>
    private Dictionary<string, AudioClip> BGMList = new Dictionary<string, AudioClip>();

    /// <summary>
    /// SE一覧
    /// </summary>
    private Dictionary<string, AudioClip> SEList = new Dictionary<string, AudioClip>();

    // Use this for initialization
    public void Start()
    {
        this.seSource = this.gameObject.AddComponent<AudioSource>();

        // BGM一覧
        for (int i = 0; i < this.bgmKey.Length; i++)
        {
            this.BGMList.Add(this.bgmKey[i], this.bgmFiles[i]);
        }

        // SE一覧
        for (int j = 0; j < this.seKey.Length; j++)
        {
            this.SEList.Add(this.seKey[j], this.seFiles[j]);
        }
    }

    /// <summary>
    /// 指定したBGMを再生する
    /// </summary>
    /// <param name="audioName">再生するBGMの名前</param>
    /// <param name="volume">音量</param>
    public void PlayBGM(string audioName, float volume = 1.0f)
    {
        // 現在のBGMキーを設定
        currentBGMKey = audioName;

        // 再生不可の場合再生しない
        if (playBGMFlag == GameData.SETTING_SOUND_OFF)
        {
            return;
        }

        // 再生中のBGMと指定されたBGMが異なる場合、BGMを変更する
        if (this.bgmSource.clip != this.BGMList[audioName])
        {
            this.bgmSource.volume = volume;
            this.bgmSource.Stop();
            this.bgmSource.clip = BGMList[audioName];

            this.bgmSource.Play();
        }
    }

    /// <summary>
    /// 指定したSEを再生する
    /// </summary>
    /// <param name="audioName"></param>
    public void PlaySE(string audioName)
    {

        // 再生不可の場合再生しない
        if (playSEFlag == GameData.SETTING_SOUND_OFF)
        {
            return;
        }

        this.seSource.PlayOneShot(SEList[audioName]);
    }

    /// <summary>
    /// 再生中のSEを停止する
    /// </summary>
    public void StopSE()
    {
        this.seSource.Stop();
        this.seSource.clip = null;
    }

    /// <summary>
    /// 再生中のBGMを停止する
    /// </summary>
    public void StopBGM()
    {
        this.bgmSource.Stop();
        this.bgmSource.clip = null;
    }

    /// <summary>
    /// BGM再生の可否を設定する
    /// </summary>
    /// <param name="flag"></param>
    public void SetBGMFlag(int flag)
    {
        playBGMFlag = flag;

        // 再生中のBGMを停止する
        if (playBGMFlag == GameData.SETTING_SOUND_OFF)
        {
            StopBGM();
        }
    }

    /// <summary>
    /// SE再生の可否を設定する
    /// </summary>
    /// <param name="flag"></param>
    public void SetSEFlag(int flag)
    {
        playSEFlag = flag;
    }

    /// <summary>
    /// BGM再生可否を取得する
    /// </summary>
    /// <returns></returns>
    public int GetBGMFlag()
    {
        return playBGMFlag;
    }

    /// <summary>
    /// SE再生可否を取得する
    /// </summary>
    /// <returns></returns>
    public int GetSEFlag()
    {
        return playSEFlag;
    }
}
