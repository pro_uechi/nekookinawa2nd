﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Linq;

public class ItemManager : SingletonMonoBehaviour<ItemManager>
{

    #region テーブル

    #region 攻撃力 / 猫じゃらし

    /// <summary>
    /// 攻撃力の値段テーブル
    /// </summary>
    private int[] _atkPriceTable =
    {
          100,   150,   200,   250,   300,
          350,   400,   500,   550,   600,
          650,   700,   750,   800,   850,
          900,   950,  1000,  1050,  1100,
         1150,  1200,  1250,  1300,  1350,
         1400,  1450,  1500,  1550,  1600,
    };

    /// <summary>
    /// 攻撃の加算量
    /// </summary>
    private int[] _atkRiseTable =
    {
          10,  10,  10,  10,  10,
          10,  10,  10,  10,  20,
          20,  20,  20,  20,  30,
          30,  30,  30,  30,  40,
          40,  40,  40,  40,  50,
          50,  50,  50,  50,  50,
    };

    /// <summary>
    /// 攻撃力の加算にかかる時間　(秒単位)
    /// </summary>
    private int[] _atkWaitTable =
    {
          60,  80, 100, 120, 140,
         160, 180, 200, 220, 240,
         260, 280, 300, 320, 340,
         360, 380, 400, 420, 440,
         460, 480, 500, 520, 540,
         560, 580, 600, 620, 640,
    };

    #endregion

    #region 守備力 / ボール

    /// <summary>
    /// 守備力の値段テーブル
    /// </summary>
    private int[] _defPriceTable =
    {
          100,   150,   200,   250,   300,
          350,   400,   500,   550,   600,
          650,   700,   750,   800,   850,
          900,   950,  1000,  1050,  1100,
         1150,  1200,  1250,  1300,  1350,
         1400,  1450,  1500,  1550,  1600,
    };

    /// <summary>
    /// 守備力の加算量
    /// </summary>
    private int[] _defRiseTable =
    {
          10,  10,  10,  10,  10,
          10,  10,  10,  10,  20,
          20,  20,  20,  20,  30,
          30,  30,  30,  30,  40,
          40,  40,  40,  40,  50,
          50,  50,  50,  50,  50,
    };

    /// <summary>
    /// 守備力の加算にかかる時間　(秒単位)
    /// </summary>
    private int[] _defWaitTable =
    {
          60,  80, 100, 120, 140,
         160, 180, 200, 220, 240,
         260, 280, 300, 320, 340,
         360, 380, 400, 420, 440,
         460, 480, 500, 520, 540,
         560, 580, 600, 620, 640,
    };

    #endregion

    #region 体力 / キャットフード

    /// <summary>
    /// 体力の値段テーブル
    /// </summary>
    private int[] _hpPriceTable =
    {
          100,   150,   200,   250,   300,
          350,   400,   500,   550,   600,
          650,   700,   750,   800,   850,
          900,   950,  1000,  1050,  1100,
         1150,  1200,  1250,  1300,  1350,
         1400,  1450,  1500,  1550,  1600,
    };

    /// <summary>
    /// 体力の加算量
    /// </summary>
    private int[] _hpRiseTable =
    {
          10,  10,  10,  10,  10,
          10,  10,  10,  10,  10,
          20,  20,  20,  20,  20,
          30,  30,  30,  30,  30,
          40,  40,  40,  40,  40,
          50,  50,  50,  50,  50,
    };

    /// <summary>
    /// 体力の加算にかかる時間　(秒単位)
    /// </summary>
    private int[] _hpWaitTable =
    {
          60,  80, 100, 120, 140,
         160, 180, 200, 220, 240,
         260, 280, 300, 320, 340,
         360, 380, 400, 420, 440,
         460, 480, 500, 520, 540,
         560, 580, 600, 620, 640,
    };

    #endregion

    #region スピード / ねずみ

    /// <summary>
    /// スピードの値段テーブル
    /// </summary>
    private int[] _spdPriceTable =
    {
          100,   150,   200,   250,   300,
          350,   400,   500,   550,   600,
          650,   700,   750,   800,   850,
          900,   950,  1000,  1050,  1100,
         1150,  1200,  1250,  1300,  1350,
         1400,  1450,  1500,  1550,  1600,
    };

    /// <summary>
    /// スピードの加算量
    /// </summary>
    private int[] _spdRiseTable =
    {
          10,  10,  10,  10,  10,
          10,  10,  10,  10,  20,
          20,  20,  20,  20,  30,
          30,  30,  30,  30,  40,
          40,  40,  40,  40,  50,
          50,  50,  50,  50,  50,
    };

    /// <summary>
    /// スピードの加算にかかる時間　(秒単位)
    /// </summary>
    private int[] _spdWaitTable =
    {
          60,  80, 100, 120, 140,
         160, 180, 200, 220, 240,
         260, 280, 300, 320, 340,
         360, 380, 400, 420, 440,
         460, 480, 500, 520, 540,
         560, 580, 600, 620, 640,
    };

    #endregion

    #endregion

    #region 列挙型

    /// <summary>
    /// どのアイテムを選んだか調べる列挙型
    /// </summary>
    public enum ITEM
    {
        _NEKOJARASHI,       // 攻撃力
        _BALL,              // 守備力
        _MOUSE,             // スピード
        _CAT_FOOD,          // 体力
        _MAX,
    }

    #endregion

    #region 定数

    /// <summary>
    /// アイテム数
    /// </summary>
    private const int MAX_ITEM_VALUE = 4;

    /// <summary>
    /// テーブルのインデックス
    /// </summary>
    private const int MAX_TABLE_VALUE = 30;


    /// <summary>
    /// 猫じゃらしを使用した際のアニメーション
    /// </summary>
    private const string ATK_ANI = "atk";

    /// <summary>
    /// ボールを使用した際のアニメーション
    /// </summary>
    private const string DEF_ANI = "def";

    /// <summary>
    /// キャットフードを使用した際のアニメーション
    /// </summary>
    private const string HP_ANI = "hp";

    /// <summary>
    /// スピードを使用した際のアニメーション
    /// </summary>
    private const string SPD_ANI = "spd";

    /// <summary>
    /// ノーマル1を使用した際のアニメーション
    /// </summary>
    private const string NORMAL_ANI_1 = "normal";

    /// <summary>
    /// ノーマル２を使用した際のアニメーション
    /// </summary>
    private const string NORMAL_ANI_2 = "normal_2";

    /// <summary>
    /// ノーマル３を使用した際のアニメーション
    /// </summary>
    private const string NORMAL_ANI_3 = "normal_3";

    #endregion

    #region 変数

    /// <summary>
    /// 待ち時間のメッセージ
    /// </summary>
    [SerializeField, Header("待ち時間の表示に使用")]
    private GameObject _waitTimeMessage = null;

    /// <summary>
    /// 待ち時間のテキスト
    /// </summary>
    [SerializeField, Header("待ち時間のテキスト")]
    private Text _waitTimeText = null;

    /// <summary>
    /// ステータスアップ報告メッセージ
    /// </summary>
    [SerializeField, Header("ステータスアップ報告メッセージ")]
    private GameObject _statusUpReportMessage = null;

    /// <summary>
    /// ステータスアップ報告テキスト
    /// </summary>
    [SerializeField, Header("ステータスアップ報告テキスト")]
    private Text _statusUpReportText = null;

    /// <summary>
    /// ステータスアップメッセージ
    /// </summary>
    [SerializeField, Header("ステータスアップメッセージ")]
    private GameObject _statusUpMessage = null;

    /// <summary>
    /// ステータスアップの上昇値テキスト
    /// </summary>
    [SerializeField, Header("ステータスアップの上昇値テキスト")]
    private Text _statusUp_UpwardText = null;

    /// <summary>
    /// 各ステータスアップのアイコン
    /// </summary>
    [SerializeField, Header("各ステータスアップのアイコン")]
    private GameObject[] _statusUpIcons = new GameObject[MAX_ITEM_VALUE];

    ///// <summary>
    ///// 各アイテムの購入に必要なスターの数
    ///// </summary>
    //[SerializeField, Header("各アイテムの購入に必要なスターの数")]
    //private int[] _itemBuyStarValues = new int[MAX_ITEM_VALUE];

    /// <summary>
    /// 各アイテム価格テキスト
    /// </summary>
    private Text[] _itemPriceTexts = new Text[MAX_ITEM_VALUE];

    ///// <summary>
    ///// 各アイテムの購入に必要なスターの数テキスト
    ///// </summary>
    //private Text[] _itemBuyStarValueTexts = new Text[MAX_ITEM_VALUE];

    /// <summary>
    /// 各アイテムを買った際の上昇値のテキスト
    /// </summary>
    private Text[] _itemBuyUpwardValueTexts = new Text[MAX_ITEM_VALUE];

    /// <summary>
    /// アイテムをコインで買うか、スターで買うかメッセージ
    /// </summary>
    private GameObject _coin_or_StarMessage;

    /// <summary>
    /// アイテム購入完了メッセージ
    /// </summary>
    private GameObject _buyItemMessage;

    /// <summary>
    /// 購入できませんメッセージ
    /// </summary>
    private GameObject _canNotBuyItemMessage;

    /// <summary>
    /// ホーム猫のアニメーター
    /// </summary>
    private Animator _catHomeAnimator;

    /// <summary>
    /// 保存されたステータスのテキストのキー
    /// </summary>
    private List<string> _saveStatusTextsKey = new List<string>();

    /// <summary>
    /// 保存されたステータスのテキストの値
    /// </summary>
    private List<int> _saveStatusTextsValue = new List<int>();

    /// <summary>
    /// 各アイテムの値段
    /// </summary>
    private int[] _itemPrices = new int[MAX_ITEM_VALUE];

    /// <summary>
    /// アニメーションの待ち時間を格納
    /// </summary>
    private int[] _aniWaitValues = new int[MAX_ITEM_VALUE];

    /// <summary>
    /// 前のランダムの値を格納
    /// </summary>
    private int _preRandam = 0;

    /// <summary>
    /// アニメーションが再生されているか？
    /// </summary>
    private bool _isAnimation = false;

    /// <summary>
    /// ステータスメッセージが表示されているか？
    /// </summary>
    private bool _isDisplayStatuMessage = false;

    /// <summary>
    /// ステータスアップメッセージの×ボタンが押されているか？
    /// </summary>
    private bool _isCloseButton = false;

    /// <summary>
    /// 合計待ち時間
    /// </summary>
    private float _waitTime = 0.0f;

    /// <summary>
    /// 再生するアニメーションの名前
    /// </summary>
    private string _playAniName = "";

    /// <summary>
    /// どのアイテムを選んだか調べる列挙型
    /// </summary>
    private ITEM _item;

    /// <summary>
    /// コルーチンを格納する変数
    /// </summary>
    private IEnumerator _playAnimationCou;

    /// <summary>
    /// 経過時間のコルーチン処理を格納する変数
    /// </summary>
    private IEnumerator _getElapseAnimation;


    #endregion

    #region getter/setter

    /// <summary>
    /// 各アイテム価格テキスト
    /// </summary>
    public Text[] ItemPriceTexts { get { return _itemPriceTexts; } set { _itemPriceTexts = value; } }

    ///// <summary>
    ///// 各アイテムの購入に必要なスターの数テキスト
    ///// </summary>
    //public Text[] ItemBuyStarValueTexts { get { return _itemBuyStarValueTexts; } set { _itemBuyStarValueTexts = value; } }

    /// <summary>
    ///　各アイテムを買った際の上昇値のテキスト
    /// </summary>
    public Text[] ItemBuyUpwardValueTexts { get { return _itemBuyUpwardValueTexts; } set { _itemBuyUpwardValueTexts = value; } }

    /// <summary>
    /// アイテムをコインで買うか、スターで買うかメッセージ
    /// </summary>
    public GameObject Coin_or_StarMessage { get { return _coin_or_StarMessage; } set { _coin_or_StarMessage = value; } }

    /// <summary>
    /// アイテム購入完了メッセージ
    /// </summary>
    public GameObject BuyItemMessage { get { return _buyItemMessage; } set { _buyItemMessage = value; } }

    /// <summary>
    /// アイテム購入できませんメッセージ
    /// </summary>
    public GameObject CanNotBuyItemMessage { get { return _canNotBuyItemMessage; } set { _canNotBuyItemMessage = value; } }


    #endregion

    #region 初期化

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="isText"> テキストを書き込むか？ </param>
    public void Initialize(bool isText = true)
    {
        // アイテムはねこじゃらし状態から始める
        _item = ITEM._NEKOJARASHI;

        // アイテムの値段初期化
        _itemPrices[(int)ITEM._NEKOJARASHI] = _atkPriceTable[(int)GameData.UserData.itemPriceIndexs[(int)ITEM._NEKOJARASHI]];
        _itemPrices[(int)ITEM._BALL] = _defPriceTable[(int)GameData.UserData.itemPriceIndexs[(int)ITEM._BALL]];
        _itemPrices[(int)ITEM._MOUSE] = _spdPriceTable[(int)GameData.UserData.itemPriceIndexs[(int)ITEM._MOUSE]];
        _itemPrices[(int)ITEM._CAT_FOOD] = _hpPriceTable[(int)GameData.UserData.itemPriceIndexs[(int)ITEM._CAT_FOOD]];

        // アニメーションの待ち時間格納
        _aniWaitValues[(int)ITEM._NEKOJARASHI] = _atkRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._NEKOJARASHI]];
        _aniWaitValues[(int)ITEM._BALL] = _defRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._BALL]];
        _aniWaitValues[(int)ITEM._MOUSE] = _spdRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._MOUSE]];
        _aniWaitValues[(int)ITEM._CAT_FOOD] = _hpRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._CAT_FOOD]];

        if (!isText) { return; }

        // アイテムの値段、アイテム購入に必要なスターの数、アイテムを買った際の上昇を描画
        for (int i = 0; i < MAX_ITEM_VALUE; i++)
        {
            _itemPriceTexts[i].text = "×\t" + _itemPrices[i].ToString();
            //_itemBuyStarValueTexts[i].text = "×" + _itemBuyStarValues[i].ToString();
        }

        // アイテムを買った際の上昇を描画
        _itemBuyUpwardValueTexts[(int)ITEM._NEKOJARASHI].text = "+" + _atkRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._NEKOJARASHI]].ToString();
        _itemBuyUpwardValueTexts[(int)ITEM._BALL].text = "+" + _defRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._BALL]].ToString();
        _itemBuyUpwardValueTexts[(int)ITEM._MOUSE].text = "+" + _spdRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._MOUSE]].ToString();
        _itemBuyUpwardValueTexts[(int)ITEM._CAT_FOOD].text = "+" + _hpRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._CAT_FOOD]].ToString();
    }

    #endregion

    #region アイテム購入処理

    ///// <summary>
    ///// アイテムの価格と必要なスター数を返す処理
    ///// </summary>
    ///// <returns></returns>
    //public (int, int) GetItemPrice_And_StarValue()
    //{
    //    switch (_item)
    //    {
    //        case ITEM._NEKOJARASHI:
    //            return (_itemPrices[(int)ITEM._NEKOJARASHI], _itemBuyStarValues[(int)ITEM._NEKOJARASHI]);
    //        case ITEM._BALL:
    //            return (_itemPrices[(int)ITEM._BALL], _itemBuyStarValues[(int)ITEM._BALL]);
    //        case ITEM._MOUSE:
    //            return (_itemPrices[(int)ITEM._MOUSE], _itemBuyStarValues[(int)ITEM._MOUSE]);
    //        case ITEM._CAT_FOOD:
    //            return (_itemPrices[(int)ITEM._CAT_FOOD], _itemBuyStarValues[(int)ITEM._CAT_FOOD]);
    //    }

    //    Debug.LogError("アイテム価格が違います。");
    //    return (-1, -1);
    //}

    public int GetItemPrice()
    {
        switch (_item)
        {
            case ITEM._NEKOJARASHI:
                return _itemPrices[(int)ITEM._NEKOJARASHI];
            case ITEM._BALL:
                return _itemPrices[(int)ITEM._BALL];
            case ITEM._MOUSE:
                return _itemPrices[(int)ITEM._MOUSE];
            case ITEM._CAT_FOOD:
                return _itemPrices[(int)ITEM._CAT_FOOD];
        }

        return -1;
    }

    /// <summary>
    /// アイテムをお金で買えるか調べる
    /// </summary>
    public bool CheckBuyItem_WithMoney()
    {
        switch (_item)
        {
            // 攻撃力
            case ITEM._NEKOJARASHI:
                if (_itemPrices[(int)ITEM._NEKOJARASHI] <= GameData.UserData.Money)
                {
                    // 猫じゃらしの値段分お金を払う
                    GameData.UserData.Money -= _itemPrices[(int)ITEM._NEKOJARASHI];
                    BeforeAnimation(ITEM._NEKOJARASHI);
                    return true;
                }
                break;
            // ディフェンス
            case ITEM._BALL:
                if (_itemPrices[(int)ITEM._BALL] <= GameData.UserData.Money)
                {
                    // ボールの値段分お金を払う
                    GameData.UserData.Money -= _itemPrices[(int)ITEM._BALL];
                    BeforeAnimation(ITEM._BALL);
                    return true;
                }
                break;
            // スピード力
            case ITEM._MOUSE:
                if (_itemPrices[(int)ITEM._MOUSE] <= GameData.UserData.Money)
                {
                    // ねずみの値段分お金を払う
                    GameData.UserData.Money -= _itemPrices[(int)ITEM._MOUSE];
                    BeforeAnimation(ITEM._MOUSE);
                    return true;
                }
                break;
            // 体力
            case ITEM._CAT_FOOD:
                if (_itemPrices[(int)ITEM._CAT_FOOD] <= GameData.UserData.Money)
                {
                    // キャットフードの値段分お金を払う
                    GameData.UserData.Money -= _itemPrices[(int)ITEM._CAT_FOOD];
                    BeforeAnimation(ITEM._CAT_FOOD);
                    return true;
                }
                break;
        }
        return false;
    }

    ///// <summary>
    ///// アイテムをスターで買えるか調べる
    ///// </summary>
    //public bool CheckBuyItem_WithStar()
    //{
    //    switch (_item)
    //    {
    //        // 攻撃力
    //        case ITEM._NEKOJARASHI:
    //            if (_itemBuyStarValues[(int)ITEM._NEKOJARASHI] <= GameData.UserData.StarCount)
    //            {
    //                // 猫じゃらしのスター価格を払う
    //                GameData.UserData.StarCount -= _itemBuyStarValues[(int)ITEM._NEKOJARASHI];
    //                BeforeAnimation(ITEM._NEKOJARASHI);
    //                return true;
    //            }
    //            break;
    //        // ディフェンス力
    //        case ITEM._BALL:
    //            if (_itemBuyStarValues[(int)ITEM._BALL] <= GameData.UserData.StarCount)
    //            {
    //                // ボールのスター価格を払う
    //                GameData.UserData.StarCount -= _itemBuyStarValues[(int)ITEM._BALL];
    //                BeforeAnimation(ITEM._BALL);
    //                return true;
    //            }
    //            break;
    //        // スピード力
    //        case ITEM._MOUSE:
    //            if (_itemBuyStarValues[(int)ITEM._MOUSE] <= GameData.UserData.StarCount)
    //            {
    //                // ねずみのスター価格を払う
    //                GameData.UserData.StarCount -= _itemBuyStarValues[(int)ITEM._MOUSE];
    //                BeforeAnimation(ITEM._MOUSE);
    //                return true;
    //            }
    //            break;
    //        // 体力
    //        case ITEM._CAT_FOOD:
    //            if (_itemBuyStarValues[(int)ITEM._CAT_FOOD] <= GameData.UserData.StarCount)
    //            {
    //                // キャットフード価格を払う
    //                GameData.UserData.StarCount -= _itemBuyStarValues[(int)ITEM._CAT_FOOD];
    //                BeforeAnimation(ITEM._CAT_FOOD);
    //                return true;
    //            }
    //            break;
    //    }
    //    return false;
    //}

    /// <summary>
    /// プレイヤーステータスの上限値を越えているか調べる（TRUE: 越えている FALSE: 越えていない）
    /// </summary>
    public bool Check_MaxPlayerStatus()
    {
        switch (_item)
        {
            case ITEM._NEKOJARASHI:
                if (GameData.UserData.MyCharacterData.ATK >= GameData.UserData.MyCharacterData.MAX_ATK)
                {
                    return true;
                }
                break;
            case ITEM._BALL:
                if (GameData.UserData.MyCharacterData.DEF >= GameData.UserData.MyCharacterData.MAX_DEF)
                {
                    return true;
                }
                break;
            case ITEM._MOUSE:
                if (GameData.UserData.MyCharacterData.SPD >= GameData.UserData.MyCharacterData.MAX_SPD)
                {
                    return true;
                }
                break;
            case ITEM._CAT_FOOD:
                if (GameData.UserData.MyCharacterData.HP >= GameData.UserData.MyCharacterData.MAX_HP)
                {
                    return true;
                }
                break;
        }
        return false;
    }

    #endregion

    #region ボタン処理

    /// <summary>
    /// 猫じゃらしを購入するボタンを押した時の処理
    /// </summary>
    public void OnClickBuyNekoJarashi()
    {
        AudioManager.Instance.PlaySE("OkSe");
        _item = ITEM._NEKOJARASHI;
        _coin_or_StarMessage.SetActive(true);
    }

    /// <summary>
    /// ボールを購入するボタンを押した時の処理
    /// </summary>
    public void OnClickBuyBall()
    {
        AudioManager.Instance.PlaySE("OkSe");
        _item = ITEM._BALL;
        _coin_or_StarMessage.SetActive(true);
    }

    /// <summary>
    /// ねずみを購入するボタンを押した時の処理
    /// </summary>
    public void OnClickBuyMouse()
    {
        AudioManager.Instance.PlaySE("OkSe");
        _item = ITEM._MOUSE;
        _coin_or_StarMessage.SetActive(true);
    }

    /// <summary>
    /// キャットフードを購入するボタンを押した時の処理
    /// </summary>
    public void OnClickBuyCatFood()
    {
        AudioManager.Instance.PlaySE("OkSe");
        _item = ITEM._CAT_FOOD;
        _coin_or_StarMessage.SetActive(true);
    }

    /// <summary>
    /// ショップ画面に戻る処理
    /// </summary>
    public void OnClickReturnShop()
    {
        AudioManager.Instance.PlaySE("OkSe");
        _buyItemMessage.SetActive(false);
        _canNotBuyItemMessage.SetActive(false);
        _coin_or_StarMessage.SetActive(false);
    }


    /// <summary>
    /// アイテムを購入してショップ画面に戻る処理
    /// </summary>
    public void OnClick_BuyItem_ReturnShop()
    {
        AudioManager.Instance.PlaySE("OkSe");
        _buyItemMessage.SetActive(false);
        _canNotBuyItemMessage.SetActive(false);
        _coin_or_StarMessage.SetActive(false);

        // アニメーションの時間設定
        SettingAnimation(_playAniName);
        // アニメーションが再生できるか調べる
        if(CheckPlayAnimation())
        {
            _playAnimationCou = PlayAnimation();
            // アニメーション再生
            StartCoroutine(_playAnimationCou);
        }
    }

    /// <summary>
    /// ステータスのメッセージの×ボタンが押された際に呼ばれる処理
    /// </summary>
    public void OnClickStatuUpMessageClose()
    {
        AudioManager.Instance.PlaySE("OkSe");
        _isCloseButton = true;
    }

    /// <summary>
    /// アニメーションの待ち時間ボタンを押した際に呼ばれる処理
    /// </summary>
    public void OnClickTimeMessage()
    {
        AudioManager.Instance.PlaySE("OkSe");
        // 広告を表示するか提示
        UnityAdsManager.Instance.ShowAd(AdType.WaitTimeHome);
    }

    #endregion

    #region ステータス変更処理

    /// <summary>
    /// 攻撃力の値を変更する
    /// </summary>
    void ChangeATK(int addAtk)
    {
        GameData.UserData.MyCharacterData.ATK += addAtk;

        // 攻撃力最大値を超えたら元に戻す処理
        if (GameData.UserData.MyCharacterData.ATK > GameData.UserData.MyCharacterData.MAX_ATK)
        {
            GameData.UserData.MyCharacterData.ATK = GameData.UserData.MyCharacterData.MAX_ATK;
        }

        SaveData.SetFloat(SaveKey.ATKData, GameData.UserData.MyCharacterData.ATK);
        SaveData.Save();
        // 上昇値描画
        ChangeStatusTextIcon(ITEM._NEKOJARASHI, addAtk);
    }

    /// <summary>
    /// 守備力の値を変更する
    /// </summary>
    void ChangeDEF(int addDef)
    {
        GameData.UserData.MyCharacterData.DEF += addDef;

        // 守備力最大値を超えたら元に戻す処理
        if (GameData.UserData.MyCharacterData.DEF > GameData.UserData.MyCharacterData.MAX_DEF)
        {
            GameData.UserData.MyCharacterData.DEF = GameData.UserData.MyCharacterData.MAX_DEF;
        }

        SaveData.SetFloat(SaveKey.DEFData, GameData.UserData.MyCharacterData.DEF);
        SaveData.Save();
        // 上昇値描画
        ChangeStatusTextIcon(ITEM._BALL, addDef);
    }

    /// <summary>
    /// スピードの値を変更する
    /// </summary>
    void ChangeSPD(int addSpd)
    {
        GameData.UserData.MyCharacterData.SPD += addSpd;

        // スピード力最大値を超えたら元に戻す処理
        if (GameData.UserData.MyCharacterData.SPD > GameData.UserData.MyCharacterData.MAX_SPD)
        {
            GameData.UserData.MyCharacterData.SPD = GameData.UserData.MyCharacterData.MAX_SPD;
        }

        SaveData.SetFloat(SaveKey.AgilityData, GameData.UserData.MyCharacterData.SPD);
        SaveData.Save();
        // 上昇値描画
        ChangeStatusTextIcon(ITEM._MOUSE, addSpd);

    }

    /// <summary>
    /// HPの値を変更する
    /// </summary>
    void ChangeHP(int addHp)
    {
        GameData.UserData.MyCharacterData.HP += addHp;

        // 体力最大値を超えたら元に戻す処理
        if (GameData.UserData.MyCharacterData.HP > GameData.UserData.MyCharacterData.MAX_HP)
        {
            GameData.UserData.MyCharacterData.HP = GameData.UserData.MyCharacterData.MAX_HP;
        }

        SaveData.SetFloat(SaveKey.HPData, GameData.UserData.MyCharacterData.HP);
        SaveData.Save();
        // 上昇値描画        
        ChangeStatusTextIcon(ITEM._CAT_FOOD, addHp);
    }

    /// <summary>
    /// 状態のテキストとアイコンの表示を変える
    /// </summary>
    void ChangeStatusTextIcon(ITEM item, int addValue)
    {
        // アイコンを非表示
        InactiveIcon();
        // アイコンを表示
        _statusUpIcons[(int)item].SetActive(true);

        string statusUpName = "";

        switch(item)
        {
            case ITEM._NEKOJARASHI:
                statusUpName = "ATK";
                break;
            case ITEM._BALL:
                statusUpName = "DEF";
                break;
            case ITEM._MOUSE:
                statusUpName = "SPD";
                break;
            case ITEM._CAT_FOOD:
                statusUpName = "HP";
                break;
        }

        // ステータスアップの上昇値描画
        _statusUp_UpwardText.text = statusUpName + "＋" + addValue + "Up";

    }

    /// <summary>
    /// アイコンを非表示にする処理
    /// </summary>
    void InactiveIcon()
    {
        foreach(var obj in _statusUpIcons)
        {
            obj.SetActive(false);
        }
    }

    /// <summary>
    /// アニメーション前に入る処理
    /// </summary>
    public void BeforeAnimation(ITEM item)
    {
        // 上昇値を描画する際の数値を格納
        int rValue = 0;
        // アニメーションをnull
        _playAniName = "null";

        // テーブルデータの最後まで到達していなければ
        if (GameData.UserData.itemRiseIndexs[(int)item] < MAX_TABLE_VALUE - 1)
        {
            /// <summary>
            /// 各状態に合わせて、アニメーション名、アニメーション待ち時間を変更する
            /// </summary>
            switch (item)
            {
                case ITEM._NEKOJARASHI:
                    _aniWaitValues[(int)ITEM._NEKOJARASHI] = _atkRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._NEKOJARASHI]];
                    _itemPrices[(int)_item] = _atkPriceTable[++GameData.UserData.itemPriceIndexs[(int)ITEM._NEKOJARASHI]];
                    GameData.UserData.aniRiseIndexs.Add((int)GameData.UserData.itemRiseIndexs[(int)ITEM._NEKOJARASHI]);
                    rValue = _atkRiseTable[++GameData.UserData.itemRiseIndexs[(int)item]];
                    _playAniName = ATK_ANI;
                    break;
                case ITEM._BALL:
                    _aniWaitValues[(int)ITEM._BALL] = _defRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._BALL]];
                    _itemPrices[(int)_item] = _defPriceTable[++GameData.UserData.itemPriceIndexs[(int)ITEM._BALL]];
                    GameData.UserData.aniRiseIndexs.Add((int)GameData.UserData.itemRiseIndexs[(int)ITEM._BALL]);
                    rValue = _defRiseTable[++GameData.UserData.itemRiseIndexs[(int)item]];
                    _playAniName = DEF_ANI;
                    break;
                case ITEM._MOUSE:
                    _aniWaitValues[(int)ITEM._MOUSE] = _spdRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._MOUSE]];
                    _itemPrices[(int)_item] = _spdPriceTable[++GameData.UserData.itemPriceIndexs[(int)ITEM._MOUSE]];
                    GameData.UserData.aniRiseIndexs.Add((int)GameData.UserData.itemRiseIndexs[(int)ITEM._MOUSE]);
                    rValue = _spdRiseTable[++GameData.UserData.itemRiseIndexs[(int)item]];
                    _playAniName = SPD_ANI;
                    break;
                case ITEM._CAT_FOOD:
                    _aniWaitValues[(int)ITEM._CAT_FOOD] = _hpRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._CAT_FOOD]];
                    _itemPrices[(int)_item] = _hpPriceTable[++GameData.UserData.itemPriceIndexs[(int)ITEM._CAT_FOOD]];
                    GameData.UserData.aniRiseIndexs.Add((int)GameData.UserData.itemRiseIndexs[(int)ITEM._CAT_FOOD]);
                    rValue = _hpRiseTable[++GameData.UserData.itemRiseIndexs[(int)item]];
                    _playAniName = HP_ANI;
                    break;
            }
        }
        // 最後まで到達していなければ
        else
        {
            /// <summary>
            /// 各状態に合わせて、アニメーション名、アニメーション待ち時間を変更する
            /// </summary>
            switch (item)
            {
                case ITEM._NEKOJARASHI:
                    _aniWaitValues[(int)ITEM._NEKOJARASHI] = _atkRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._NEKOJARASHI]];
                    _itemPrices[(int)_item] = _atkPriceTable[GameData.UserData.itemPriceIndexs[(int)ITEM._NEKOJARASHI]];
                    GameData.UserData.aniRiseIndexs.Add((int)GameData.UserData.itemRiseIndexs[(int)ITEM._NEKOJARASHI]);
                    rValue = _atkRiseTable[GameData.UserData.itemRiseIndexs[(int)item]];
                    _playAniName = ATK_ANI;
                    break;
                case ITEM._BALL:
                    _aniWaitValues[(int)ITEM._BALL] = _defRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._BALL]];
                    _itemPrices[(int)_item] = _defPriceTable[GameData.UserData.itemPriceIndexs[(int)ITEM._BALL]];
                    GameData.UserData.aniRiseIndexs.Add((int)GameData.UserData.itemRiseIndexs[(int)ITEM._BALL]);
                    rValue = _defRiseTable[GameData.UserData.itemRiseIndexs[(int)item]];
                    _playAniName = DEF_ANI;
                    break;
                case ITEM._MOUSE:
                    _aniWaitValues[(int)ITEM._MOUSE] = _spdRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._MOUSE]];
                    _itemPrices[(int)_item] = _spdPriceTable[GameData.UserData.itemPriceIndexs[(int)ITEM._MOUSE]];
                    GameData.UserData.aniRiseIndexs.Add((int)GameData.UserData.itemRiseIndexs[(int)ITEM._MOUSE]);
                    rValue = _spdRiseTable[GameData.UserData.itemRiseIndexs[(int)item]];
                    _playAniName = SPD_ANI;
                    break;
                case ITEM._CAT_FOOD:
                    _aniWaitValues[(int)ITEM._CAT_FOOD] = _hpRiseTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._CAT_FOOD]];
                    _itemPrices[(int)_item] = _hpPriceTable[GameData.UserData.itemPriceIndexs[(int)ITEM._CAT_FOOD]];
                    GameData.UserData.aniRiseIndexs.Add((int)GameData.UserData.itemRiseIndexs[(int)ITEM._CAT_FOOD]);
                    rValue = _hpRiseTable[GameData.UserData.itemRiseIndexs[(int)item]];
                    _playAniName = HP_ANI;
                    break;
            }
        }

        // テキストに描画
        _itemBuyUpwardValueTexts[(int)item].text = "+" + rValue.ToString();
        _itemPriceTexts[(int)item].text = "×\t" + _itemPrices[(int)item].ToString();
        // アイテムの値段を保存
        SaveData.SetList<int>(SaveKey.PriceValueData, GameData.UserData.itemPriceIndexs);
        // 上昇値データを保存
        SaveData.SetList<int>(SaveKey.RiseValueData, GameData.UserData.itemRiseIndexs);
        // 上昇値保存
        SaveData.SetList<int>(SaveKey.AniRiseIndexs, GameData.UserData.aniRiseIndexs);
        SaveData.Save();
    }

    /// <summary>
    /// 各ステータスの値を変える
    /// </summary>
    public void ChangeStatus(string aniName)
    {
        // ステータスアップメッセージ表示
        if (!_statusUpMessage.activeSelf) { _statusUpMessage.SetActive(true); }

        switch (aniName)
        {
            case ATK_ANI:
                ChangeATK(_atkRiseTable[(int)GameData.UserData.aniRiseIndexs[0]]);
                break;
            case DEF_ANI:
                ChangeDEF(_defRiseTable[(int)GameData.UserData.aniRiseIndexs[0]]);
                break;
            case SPD_ANI:
                ChangeSPD(_spdRiseTable[(int)GameData.UserData.aniRiseIndexs[0]]);
                break;
            case HP_ANI:
                ChangeHP(_hpRiseTable[(int)GameData.UserData.aniRiseIndexs[0]]);
                break;
        }
    }

    /// <summary>
    /// ホーム画面以外でステータスが上昇した際に入る処理
    /// </summary>
    void ChangeRiseStatusText()
    {

        while (_saveStatusTextsKey.Count > 0 && !_isAnimation)
        {
            // ステータスメッセージセット
            SettingStatusMessage(_saveStatusTextsKey[0]);
            _saveStatusTextsKey.RemoveAt(0);
            _saveStatusTextsValue.RemoveAt(0);
        }

        // ステータスが表示されていなければ
        if (!IsDisplayStatusMessage())
        {
            // ステータス表示
            StartCoroutine(DisplayStatusMessage());
        }
    }

    #endregion

    #region アニメーション

    /// <summary>
    /// ゲーム開始時に呼ばれるアニメーション(ホームシーンで呼ばれる)
    /// </summary>
    public void StartAnimation(int time)
    {
        // すでにホームシーンならこの処理を読まない
        if (GameData.IsHome) { return; }
       
        _getElapseAnimation = GetElapseAnimation(time);
        // コルーチン開始
        StartCoroutine(_getElapseAnimation);
        GameData.IsHome = true;
    }

    /// <summary>
    /// 経過時間を取得して、アニメーション再生
    /// </summary>
    public IEnumerator GetElapseAnimation(int elapseTime)
    {
        // 経過時間からアニメーション時間を引く処理
        CheckAnimation(elapseTime);

        ChangeRiseStatusText();

        // アニメーションが再生できるか調べる
        if (CheckPlayAnimation())
        {
            // Debug.LogError("バトルからホーム");
            // コルーチンを格納
            _playAnimationCou = PlayAnimation();
            // アニメーション再生
            StartCoroutine(_playAnimationCou);
        }
        yield break;
    }

    /// <summary>
    /// アニメーションの時間を設定
    /// </summary>
    private void SettingAnimation(string aniName)
    {
        // アニメーションがnullなら処理を抜ける
        if (aniName == "null") { return; }

        // 倍率を格納
        float magnification = 0;

        switch (aniName)
        {
            case ATK_ANI:
                magnification = _atkWaitTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._NEKOJARASHI]];
                break;
            case DEF_ANI:
                magnification = _defWaitTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._BALL]];
                break;
            case SPD_ANI:
                magnification = _spdWaitTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._MOUSE]];
                break;
            case HP_ANI:
                magnification = _hpWaitTable[(int)GameData.UserData.itemRiseIndexs[(int)ITEM._CAT_FOOD]];
                break;
        }

        //Debug.LogError(aniName + ":" + magnification);
        // アニメーション時間を設定
        float animationTime = magnification;
        // 各アニメーション名前格納
        GameData.UserData.aniNames.Add(aniName);
        // 各アニメーション時間格納
        GameData.UserData.aniTimes.Add(animationTime);
        // トータルのアニメーション時間を計測
        _waitTime += animationTime;
        // データを保存
        SaveAnimation();
    }

    /// <summary>
    /// アニメーションを再生できるか調べる (TRUE: 再生できる FALSE: 再生できない)
    /// </summary>
    private bool CheckPlayAnimation()
    {
        // アニメーションがあり、アニメーションが再生されていなければTRUEを返す
        if(GameData.UserData.aniNames.Count > 0 && !_isAnimation)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// アニメーション再生
    /// </summary>
    private IEnumerator PlayAnimation()
    {
        int fps = 0;

        _isAnimation = true;
        _waitTimeMessage.SetActive(true);

        // 格納されている全てのアニメーションが再生されるまでループ
        while (true)
        {
            // アニメーターを設定
            SetAnimator();

            // 一番最初に格納されているアニメーション再生
            while (GameData.UserData.aniTimes[0] > 0.0f)
            {

                // 一秒経ったら引いていく
                if (++fps >= GameData.SECOND)
                {
                    --GameData.UserData.aniTimes[0];
                    --_waitTime;
                    fps = 0;            
                    SaveAnimation();        // データ保存
                }
                // 待ち時間を表示
                DisplayWaitTime();
                yield return null;
            }

            // ステータスメッセージを表示
            //StartCoroutine(DisplayStatusMessage(GameData.UserData.aniNames[0]));
            //// ステータス変更
            //ChangeStatus(GameData.UserData.aniNames[0]);

            // ステータスメッセージセット
            SettingStatusMessage(GameData.UserData.aniNames[0]);

            // ステータスが表示されていなければ
            if (!IsDisplayStatusMessage())
            {
                // ステータス表示
                StartCoroutine(DisplayStatusMessage());
            }

            _catHomeAnimator.SetBool(GameData.UserData.aniNames[0], false);

            // 一番最初の要素削除
            RemoveFirstAnimation();
            // データ保存
            SaveAnimation();

            // アニメーションが無ければ、処理を抜ける
            if (GameData.UserData.aniNames.Count <= 0)
            {

                _waitTimeMessage.SetActive(false);
                _isAnimation = false;
                yield break;
            }

            yield return null;
        }
    }

    /// <summary>
    /// アニメータをセットする
    /// </summary>
    public void SetAnimator()
    {
        _catHomeAnimator = GameData.HomePlayerObject.GetComponent<Animator>();
        _catHomeAnimator.keepAnimatorControllerStateOnDisable = true;

        if (GameData.UserData.aniNames.Count > 0)
        {
            _catHomeAnimator.SetBool(GameData.UserData.aniNames[0], true);
        }
    }

    /// <summary>
    /// アニメーションのトータル時間を調べる
    /// </summary>
    private void CheckAnimation(int elapsedTime)
    {

        // アニメーションがあり、経過時間が０以上ならループする
        while (GameData.UserData.aniNames.Count > 0 && elapsedTime > 0)
        {
            // 経過時間がアニメーションの時間より長ければ、アニメーション削除
            if (GameData.UserData.aniTimes[0] < elapsedTime)
            {
                //Debug.LogError(GameData.UserData.aniNames[0] + "削除");
                elapsedTime -= (int)GameData.UserData.aniTimes[0];

                int riseValue = 0;
                // アニメーションに応じてステータス上昇
                switch (GameData.UserData.aniNames[0])
                {
                    case ATK_ANI:
                        riseValue = _atkRiseTable[(int)GameData.UserData.aniRiseIndexs[0]];
                        break;
                    case DEF_ANI:
                        riseValue = _defRiseTable[(int)GameData.UserData.aniRiseIndexs[0]];
                        break;
                    case SPD_ANI:
                        riseValue = _spdRiseTable[(int)GameData.UserData.aniRiseIndexs[0]];
                        break;
                    case HP_ANI:
                        riseValue = _hpRiseTable[(int)GameData.UserData.aniRiseIndexs[0]];
                        break;
                }
                _saveStatusTextsKey.Add(GameData.UserData.aniNames[0]);
                _saveStatusTextsValue.Add(riseValue);
                RemoveFirstAnimation();
            }
            // じゃなければアニメーションの時間を引いて終了
            else
            {
                GameData.UserData.aniTimes[0] -= elapsedTime;
                //Debug.LogError("アニメーション時間が経過時間を上回りました。");
                break;
            }
        }

        // アニメーションのトータル時間を計測
        foreach (var time in GameData.UserData.aniTimes)
        {
            _waitTime += time;
        }

        // 保存
        SaveAnimation();
    }

    /// <summary>
    /// アニメーションに関するデータを保存する処理
    /// </summary>
    private void SaveAnimation()
    {
        // データを保存
        SaveData.SetList<string>(SaveKey.AniNames, GameData.UserData.aniNames);
        SaveData.SetList<float>(SaveKey.AniTimes, GameData.UserData.aniTimes);
        SaveData.Save();
    }

    /// <summary>
    /// 最初に格納されているアニメーション情報を削除
    /// </summary>
    private void RemoveFirstAnimation()
    {
        GameData.UserData.aniTimes.RemoveAt(0);
        GameData.UserData.aniNames.RemoveAt(0);
    }

    #endregion

    #region その他

    /// <summary>
    /// ステータスメッセージに表示する名前を設定
    /// </summary>
    /// <param name="sName"></param>
    private void SettingStatusMessage(string sName)
    {
        GameData.UserData.statusMessageName.Add(sName);
        SaveData.SetList<string>(SaveKey.StatusMessageName, GameData.UserData.statusMessageName);
        SaveData.Save();
    }

    /// <summary>
    /// ステータスメッセージが表示されているか？（TRUE: 表示されている FALSE: 表示されていない）
    /// </summary>
    private bool IsDisplayStatusMessage()
    {
        if(GameData.UserData.statusMessageName.Count > 0 && !_isDisplayStatuMessage)
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// ステータスメッセージを表示
    /// </summary>
    private IEnumerator DisplayStatusMessage()
    {

        _isDisplayStatuMessage = true;
        _statusUpReportMessage.SetActive(true);

        // ループ
        while(GameData.UserData.statusMessageName.Count > 0 && _isDisplayStatuMessage)
        {
            string aniText = "";

            // アニメーションに応じてテキスト変更
            switch (GameData.UserData.statusMessageName[0])
            {
                case ATK_ANI:
                    aniText = "ATK";
                    break;
                case DEF_ANI:
                    aniText = "DEF";
                    break;
                case SPD_ANI:
                    aniText = "SPD";
                    break;
                case HP_ANI:
                    aniText = "HP";
                    break;
            }

            _statusUpReportText.text = aniText + "が上がりました！！";

            // ステータスアップのメッセージの×ボタンが押されていなけば、ループ
            while (!_isCloseButton)
            {
                //Debug.LogError("×ボタンを押せ！！");
                yield return null;
            }

            // ステータス変更
            ChangeStatus(GameData.UserData.statusMessageName[0]);
            GameData.UserData.aniRiseIndexs.RemoveAt(0);
            GameData.UserData.statusMessageName.RemoveAt(0);
            SaveData.SetList<int>(SaveKey.AniRiseIndexs, GameData.UserData.aniRiseIndexs);
            SaveData.SetList<string>(SaveKey.StatusMessageName, GameData.UserData.statusMessageName);
            SaveData.Save();
            _isCloseButton = false;
            yield return null;
        }

        _isDisplayStatuMessage = false;
        _statusUpReportMessage.SetActive(false);
    }

    /// <summary>
    /// アイテムの待ち時間を表示
    /// </summary>
    private void DisplayWaitTime()
    {
        
        int hour = Mathf.FloorToInt(_waitTime / 3600);
        int minutes = Mathf.FloorToInt((_waitTime / 60) - hour * 60);
        int seconds = Mathf.FloorToInt(_waitTime - (minutes * 60 + hour * 3600));

        // 時間:分:秒数表示
        if (hour != 0)
        {
            _waitTimeText.text = string.Format("{0:00}:{1:00}:{2:00}", hour, minutes, seconds);
            return;
        }
        // 分:秒数表示
        else 
        {
            _waitTimeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
            return;
        }
        //// 秒数だけ表示
        //else
        //{
        //    _waitTimeText.text = string.Format("{0:00}", seconds);
        //    return;
        //}
    }

    /// <summary>
    /// ホーム画面を離れる際によばれる処理
    /// </summary>
    public void LeaveHomeInitialize()
    {

        _waitTimeMessage.SetActive(false);
        _statusUpMessage.SetActive(false);
        _statusUpReportMessage.SetActive(false);
        GameData.IsHome = false;
        SaveData.SetDateTime(SaveKey.HomeTime, DateTime.Now);
        SaveData.Save();


        if (_catHomeAnimator)
        {
            // GameObjectが非アクティブの場合でもアニメーター状態を保存
            _catHomeAnimator.keepAnimatorControllerStateOnDisable = true;

            // アニメーションがあれば、falseにする
            if (GameData.UserData.aniNames.Count > 0)
            {
                _catHomeAnimator.SetBool(GameData.UserData.aniNames[0], false);
            }
        }

        // アニメーション関係初期化
        if (_playAnimationCou != null)
        {
            StopCoroutine(_playAnimationCou);
            _playAnimationCou = null;
            _isAnimation = false;
        }

        // コルーチンの中身が格納されていたら
        if (_getElapseAnimation != null)
        {
            StopCoroutine(_getElapseAnimation);
            _getElapseAnimation = null;
        }

        _waitTime = 0;
    }


    private void Update()
    {
        // ホーム画面なら
        if (GameData.IsHome && !_isAnimation)
        {
            GameTime();
        }
    }

    private void GameTime()
    {

        if (!_catHomeAnimator)
        {
            _catHomeAnimator = GameData.HomePlayerObject.GetComponent<Animator>();
            _catHomeAnimator.keepAnimatorControllerStateOnDisable = true;
        }


        // ノーマルアニメーションが終了したら、
        if (FinishedNormalAnim())
        {
            int randam = 0;

            while (true)
            {
                randam = UnityEngine.Random.Range(1, 4);

                // ランダムの値が前のランダム値と違けれぇば処理を抜ける
                if (_preRandam != randam)
                {
                    _preRandam = randam;
                    break;
                }
            }

            switch (randam)
            {
                case 1:
                    _catHomeAnimator.SetBool(NORMAL_ANI_1, false);
                    _catHomeAnimator.SetBool(NORMAL_ANI_2, false);
                    _catHomeAnimator.SetBool(NORMAL_ANI_3, false);
                    _catHomeAnimator.SetBool(NORMAL_ANI_1, true);
                    break;
                case 2:
                    _catHomeAnimator.SetBool(NORMAL_ANI_1, false);
                    _catHomeAnimator.SetBool(NORMAL_ANI_2, false);
                    _catHomeAnimator.SetBool(NORMAL_ANI_3, false);
                    _catHomeAnimator.SetBool(NORMAL_ANI_2, true);
                    break;
                case 3:
                    _catHomeAnimator.SetBool(NORMAL_ANI_1, false);
                    _catHomeAnimator.SetBool(NORMAL_ANI_2, false);
                    _catHomeAnimator.SetBool(NORMAL_ANI_3, false);
                    _catHomeAnimator.SetBool(NORMAL_ANI_3, true);
                    break;
            }

            //Debug.LogError("ノーマルアニメーション変更しました");
        }


    }



    /// <summary>
    /// ノーマルアニメーション終了判定
    /// </summary>
    public bool FinishedNormalAnim()
    {

        float normalizedTime = _catHomeAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        if (normalizedTime >= 1)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// ノーマルアニメーション中か？(TRUE: ノーマルアニメーション再生中 FALSE: それ以外)
    /// </summary>
    public bool IsNormalAnimations()
    {

        // ノーマルアニメーション中ならtrue
        if (_catHomeAnimator.GetCurrentAnimatorStateInfo(0).IsName(NORMAL_ANI_1) ||
            _catHomeAnimator.GetCurrentAnimatorStateInfo(0).IsName(NORMAL_ANI_2) ||
            _catHomeAnimator.GetCurrentAnimatorStateInfo(0).IsName(NORMAL_ANI_3) 
           )
        {
            
            return true;
        }

        return false;
    }

    #endregion

}
