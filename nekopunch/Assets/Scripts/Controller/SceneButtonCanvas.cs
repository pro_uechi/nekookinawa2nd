﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class SceneButtonCanvas : SingletonMonoBehaviour<SceneButtonCanvas>
{
    [SerializeField]
    private Button homeButton = null;
    [SerializeField]
    private Button avaterButton = null;
    [SerializeField]
    private Button battleButton = null;
    [SerializeField]
    private Button rankingButton = null;
    [SerializeField]
    private Button pictureBookButton = null;


    void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        homeButton.onClick.AddListener(delegate { OnClickSceneButton(GameData.Scene_Home); });
        avaterButton.onClick.AddListener(delegate { OnClickSceneButton(GameData.Scene_Avatar); });
        battleButton.onClick.AddListener(delegate { OnClickSceneButton(GameData.Scene_Battle); });
        //rankingButton.onClick.AddListener(delegate { OnClickSceneButton(GameData.Scene_Ranking); });
        //pictureBookButton.onClick.AddListener(delegate { OnClickSceneButton(GameData.Scene_PictureBook); });
    }

    /// <summary>
    /// SceneButtonCanvasのオンオフ
    /// </summary>
    /// <param name="flg"></param>
    public void Enable(bool flag)
    {
        gameObject.SetActive(flag);
    }

    /// <summary>
    /// ボタンの状態を設定
    /// </summary>
    /// <param name="flag"></param>
    public void ButtonActive(bool flag)
    {
        homeButton.interactable = flag;
        avaterButton.interactable = flag;
        battleButton.interactable = flag;
        rankingButton.interactable = flag;
        pictureBookButton.interactable = flag;
    }

    /// <summary>
    /// シーンボタン押下時
    /// </summary>
    /// <param name="sceneName"></param>
    public void OnClickSceneButton(string sceneName)
    {
        Scene currentScene = SceneManager.GetActiveScene();

        // 現在のシーンと異なるシーンに遷移する場合
        if (currentScene.name != sceneName)
        {
            // ランキングシーンの場合はコルーチンを止める
            //if (currentScene.name == GameData.Scene_Ranking)
            //{
            //   GameObject.Find("Ranking").GetComponent<Ranking>().StopCoroutines();
            //}

            // 着せ替えシーンの場合は着せ替えチェック
            if (currentScene.name == GameData.Scene_Avatar)
            {
                CheckEquip(sceneName);
            }
            // バトルの場合はバトルモードによってポップアップを表示
            else if (sceneName == GameData.Scene_Battle)
            {
                SceneFadeManager.Instance.Load(sceneName, GameData.FadeSpeed);
            }
        }
    }


    /// <summary>
    /// 着せ替えチェック
    /// </summary>
    private void CheckEquip(string sceneName)
    {
        AudioManager.Instance.PlaySE("OkSe");
        Scene currentScene = SceneManager.GetActiveScene();

		SceneFadeManager.Instance.Load(sceneName, GameData.FadeSpeed);
    }

    /// <summary>
    /// 衣装を確定してから遷移
    /// </summary>
    private void ConfirmAndGoToScene(string sceneName)
    {
        //EquipManager.Instance.ConfirmEquip();
        SceneFadeManager.Instance.Load(sceneName, GameData.FadeSpeed);
    }

    /// <summary>
    /// バトルモードチェック
    /// </summary>
    private void CheckBattleMode()
    {
        //if (GameData.BattleMode == BattleMode.Exam)
        //{
        //    GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        //    msgBox.GetComponent<MessageBoxManager>().Initialize_OK(
        //        "ランクバトルを始めます。\n" + GameData.numGameToRankConfirm + "人連勝すると\nランクがアップします。", delegate { SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed); }
        //        );
        //}
        //else
        {
            SceneFadeManager.Instance.Load(GameData.Scene_Battle, GameData.FadeSpeed);
        }
    }
}
