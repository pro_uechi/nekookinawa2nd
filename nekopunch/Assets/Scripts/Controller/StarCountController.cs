﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class StarCountController : MonoBehaviour
{
    ///// <summary>
    ///// ゲーム内通貨を表示するテキスト
    ///// </summary>
    //private TextMeshProUGUI StarCountText;

    ///// <summary>
    ///// 演出用
    ///// </summary>
    //private int workStar = 0;
    //private Animation anim;

    ///// <summary>
    ///// 所持金UPDATE管理フラグ
    ///// </summary>
    //private bool updateFlg = false;

    //// Use this for initialization
    //void Start()
    //{
    //    // 所持金を表示
    //    this.workStar = GameData.UserData.StarCount;
    //    this.StarCountText = this.transform.Find("StarText").GetComponent<TextMeshProUGUI>();
    //    this.StarCountText.text = string.Format("{0:#,0}", workStar);
    //    this.anim = this.GetComponent<Animation>();
    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    if (GameData.UserData != null)
    //    {
    //        if (GameData.UserData.StarCount != workStar)
    //        {
    //            // 値が変わった瞬間に一度だけ実行
    //            if (!updateFlg)
    //            {
    //                this.updateFlg = true;
    //                workStar = GameData.UserData.StarCount;
    //                // スターの数を保存（山城）
    //                SaveData.SetInt(SaveKey.starData, workStar);
    //                SaveData.Save();
    //                this.StarCountText.text = string.Format("{0:#,0}", workStar);
    //                this.anim.Play();
    //            }
    //        }
    //        else
    //        {
    //            this.updateFlg = false;
    //        }
    //    }

    //}
}
