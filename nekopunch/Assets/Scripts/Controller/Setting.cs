﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Setting : MonoBehaviour
{
    /// <summary>
    /// BGMボタン
    /// </summary>
    [SerializeField]
    private GameObject BGMOnButton;
    [SerializeField]
    private GameObject BGMOffButton;

    /// <summary>
    /// SEボタン
    /// </summary>
    [SerializeField]
    private GameObject SEOnButton;
    [SerializeField]
    private GameObject SEOffButton;

    [SerializeField]
    private Sprite onSprite = null;
    [SerializeField]
    private Sprite offSprite = null;

    [SerializeField]
    private GameObject closeSettingButton = null;

    /// <summary>
    /// キャラクター一覧のオブジェクト
    /// </summary>
    [SerializeField]
    private GameObject characterList;
    private GameObject CharacterListContent;

    /// <summary>
    /// クレジット一覧
    /// </summary>
    [SerializeField]
    private GameObject creditsList = null;

    /// <summary>
    /// 各メールのタイトル名
    /// </summary>
    private const string MAILTITLE_FAILER = " 不具合";
    private const string MAILTITLE_REQUEST = " 要望";
    private const string MAILTITLE_ETC = " その他";

    private const string ANNOUNCE_FAILER = " 不具合の内容をご記入ください";
    private const string ANNOUNCE_REQUEST = " ご意見・ご要望の内容をご記入ください";
    private const string ANNOUNCE_ETC = " その他の内容をご記入ください";

    /// <summary>
    /// メールのポップアップオブジェクト
    /// </summary>
    public GameObject MailPopUp;

    /// <summary>
    /// ユーザーID
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI userIDText = null;

    /// <summary>
    /// ユーザーネームテキスト
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI userNameText = null;


    /// <summary>
    /// Webビュー
    /// </summary>
    private WebViewObject m_webViewObject;

    //private const string URL = "https://policies.google.com/?hl=ja";
    private const string URL = "http://prolead.co.jp/transaction/privacy.html";
    private int MARGIN_X = Screen.width / 9;
    private int MARGIN_Y = Screen.height / 5;
    private int OUT_MARGIN_AJUST = 2000;

    /// <summary>
    /// 引き継ぎIDInput
    /// </summary>
    [SerializeField]
    private GameObject IDInput = null;


    private void Start()
    {
        // 初期ボタン設定
        if (AudioManager.Instance.GetBGMFlag() == GameData.SETTING_SOUND_ON)
        {
            BGMOnButton.GetComponent<Image>().sprite = onSprite;
            BGMOffButton.GetComponent<Image>().sprite = offSprite;
        }
        else
        {
            BGMOnButton.GetComponent<Image>().sprite = offSprite;
            BGMOffButton.GetComponent<Image>().sprite = onSprite;
        }

        if (AudioManager.Instance.GetSEFlag() == GameData.SETTING_SOUND_ON)
        {
            SEOnButton.GetComponent<Image>().sprite = onSprite;
            SEOffButton.GetComponent<Image>().sprite = offSprite;
        }
        else
        {
            SEOnButton.GetComponent<Image>().sprite = offSprite;
            SEOffButton.GetComponent<Image>().sprite = onSprite;
        }

        InitButton();
        SetUserInfo();

        ////Webビューで表示するページを先読み
        //LoadWebView(URL);
    }


    private void InitButton()
    {
        SupportPopUpEnable(false);

        //CharacterListContent = GameObject.Find("Canvas_UI/SafeArea/CharacterList/Scroll View/Viewport/Content").gameObject;
        //characterList.SetActive(false);
    }

    /// <summary>
    /// IDと名前を設定する
    /// </summary>
    private void SetUserInfo()
    {
        userIDText.text = "XZ" + GameData.UserData.UserId + "HP";
        //userNameText.text = GameData.UserData.UserName;
    }

    private void Update()
    {
        ////シーン読み込み中あれば、WebViewを消す(バトルシーンに移動時のみに必要)
        //m_webViewObject.SetVisibility(!SceneFadeManager.SceneLoadFlg);
    }

    /// <summary>
    /// BGMOnボタン押下時
    /// </summary>
    public void BGMOnButtonOnClick()
    {
        // 設定をセーブデータに記録
        SaveData.SetInt(SaveKey.BGMSetting, GameData.SETTING_SOUND_ON);
        SaveData.Save();
        BGMOnButton.GetComponent<Image>().sprite = onSprite;
        BGMOffButton.GetComponent<Image>().sprite = offSprite;

        AudioManager.Instance.SetBGMFlag(GameData.SETTING_SOUND_ON);

        // 現在のBGMを再生
        AudioManager.Instance.PlayBGM(AudioManager.Instance.currentBGMKey);
    }

    /// <summary>
    /// BGMOffボタン押下時
    /// </summary>
    public void BGMOffButtonOnClick()
    {
        // 設定をセーブデータに記録
        SaveData.SetInt(SaveKey.BGMSetting, GameData.SETTING_SOUND_OFF);
        SaveData.Save();
        BGMOnButton.GetComponent<Image>().sprite = offSprite;
        BGMOffButton.GetComponent<Image>().sprite = onSprite;

        AudioManager.Instance.SetBGMFlag(GameData.SETTING_SOUND_OFF);
    }

    /// <summary>
    /// SEOnボタン押下時
    /// </summary>
    public void SEOnButtonOnClick()
    {
        // 設定をセーブデータに記録
        SaveData.SetInt(SaveKey.SESetting, GameData.SETTING_SOUND_ON);
        SaveData.Save();
        SEOnButton.GetComponent<Image>().sprite = onSprite;
        SEOffButton.GetComponent<Image>().sprite = offSprite;

        AudioManager.Instance.SetSEFlag(GameData.SETTING_SOUND_ON);
    }

    /// <summary>
    /// SEOffボタン押下時
    /// </summary>
    public void SEOffButtonOnClick()
    {
        // 設定をセーブデータに記録
        SaveData.SetInt(SaveKey.SESetting, GameData.SETTING_SOUND_OFF);
        SaveData.Save();
        SEOnButton.GetComponent<Image>().sprite = offSprite;
        SEOffButton.GetComponent<Image>().sprite = onSprite;

        AudioManager.Instance.SetSEFlag(GameData.SETTING_SOUND_OFF);
    }

    /// <summary>
    /// 利用規約ボタン押下時
    /// </summary>
    public void LegalButtonOnClick()
    {
        Application.OpenURL(GameData.REGAL_URL);
    }

    /// <summary>
    /// レビューボタン押下時
    /// </summary>
    public void ReviewButtonOnClick()
    {
#if UNITY_ANDROID
        Application.OpenURL(GameData.REVIEW_URL_ANDROID);
#else
        Application.OpenURL(GameData.REVIEW_URL_IPHONE);
#endif
    }

    /// <summary>
    /// 不具合ボタン押下時
    /// </summary>
    public void SupportFaliureButtonOnClick()
    {
        OpenMailer.OpenSupportMail(MAILTITLE_FAILER, ANNOUNCE_FAILER);
    }

    /// <summary>
    /// 要望ボタン押下時
    /// </summary>
    public void SupportRequestButtonOnClick()
    {
        OpenMailer.OpenSupportMail(MAILTITLE_REQUEST, ANNOUNCE_REQUEST);
    }

    /// <summary>
    /// その他ボタン押下時
    /// </summary>
    public void SupportEtcButtonOnClick()
    {
        OpenMailer.OpenSupportMail(MAILTITLE_ETC, ANNOUNCE_ETC);
    }

    /// <summary>
    /// メールのポップアップを閉じる
    /// </summary>
    public void SupportPopUpEnable(bool flg)
    {
        MailPopUp.SetActive(flg);
    }

    /// <summary>
    /// 名前変更ボタン押下時
    /// </summary>
    public void ReNameButtonOnClick()
    {
        GameObject parent = GameObject.Find("Canvas_UI/SafeArea");
       // UserDataManager.Instance.SetUserName(OkAction, parent);
    }


    /// <summary>
    /// クレジットボタン押下時
    /// </summary>
    public void CreditsButtonOnClick()
    {
        creditsList.SetActive(true);
        closeSettingButton.SetActive(false);
    }

    /// <summary>
    /// クレジットを閉じるボタン押下時
    /// </summary>
    public void CloseCreditsButtonOnClick()
    {
        creditsList.SetActive(false);
        closeSettingButton.SetActive(true);
    }

    /// <summary>
    /// プライポリシー押下時
    /// </summary>
    public void PrivacyPolicyOnClick()
    {

        Application.OpenURL(URL);
        //#if UNITY_EDITOR_WIN
        //        // MacでないとUnity上でデバックできないため
        //        Application.OpenURL(URL);

        //#else
        //        SetWebViewDisplay();
        //#endif
    }

    /// <summary>
    /// 特定商取引法に基づく表示ボタン押下時
    /// </summary>
    public void TradeLawButton_OnClick()
    {
        Application.OpenURL("http://prolead.co.jp/transaction/agreement.html");
    }

    /// <summary>
    /// データ引き継ぎボタン押下時
    /// </summary>
    public void DataTransferButtonOnClick()
    {
        IDInput.SetActive(true);
    }

    /// <summary>
    /// OKボタン押下時
    /// </summary>
    public void IDInput_OKButtonOnClick()
    {
        StartCoroutine(DataTransfer());
    }

    /// <summary>
    /// キャンセルボタン押下時
    /// </summary>
    public void IDInput_CancelButtonOnClick()
    {
        IDInput.SetActive(false);
    }

    /// <summary>
    /// データ引き継ぎ
    /// </summary>
    /// <returns></returns>
    private IEnumerator DataTransfer()
    {
        // 入力されたIDがデータベースに存在するかチェック
        string id = IDInput.transform.Find("Base/InputField").GetComponent<InputField>().text;
        yield return StartCoroutine(DBAccessManager.Instance.GetTransferData(id));

        // IDの有無によって処理を分岐
        if (DBAccessManager.canTransfer)
        {
            // ローカルのユーザーIDを書き換えてセーブ
            //SaveData.SetInt(SaveKey.UserId, GameData.ConvertStringToUserID(id));



            SaveData.Save();

            // ポップアップ
            RebootMessage();
        }
        else
        {
            GameObject msgBox = Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
            msgBox.GetComponent<MessageBoxManager>().Initialize_OK("コードが違います。", null);
        }
    }

    /// <summary>
    /// 再起動メッセージ
    /// </summary>
    private void RebootMessage()
    {
        GameObject msgBox = Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        msgBox.GetComponent<MessageBoxManager>().Initialize_OK("引き継ぎ完了!\nアプリを再起動してください。", RebootMessage);
    }

    /// <summary>
    /// 引継ぎID発行ボタン押下時
    /// </summary>
    public void IssueDataTransferIDButtonOnClick()
    {
        StartCoroutine(IssueDataTransferID());
    }

    /// <summary>
    /// 引継ぎID発行
    /// </summary>
    /// <returns></returns>
    private IEnumerator IssueDataTransferID()
    {
        string id = GameData.ConvertUserIDToString(GameData.UserData.UserId);
        yield return StartCoroutine(DBAccessManager.Instance.InsertTransferData(id));

        GameObject msgBox = Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        msgBox.GetComponent<MessageBoxManager>().Initialize_OK("引継ぎコード\n<color=#FF0000>" + id + "</color>\n大切に保管してください。\n有効期間は14日です", null);
    }

    /// <summary>
    /// タイトルに戻るボタン押下時
    /// </summary>
    public void ReturnTitleOnClick()
    {
        SceneFadeManager.Instance.Load(GameData.Scene_Splash, GameData.FadeSpeed);
    }

    /// <summary>
    /// WebViewを画面上に表示
    /// </summary>
    private void SetWebViewDisplay()
    {
        // WebView のマージンを設定します
        m_webViewObject.SetMargins(MARGIN_X, MARGIN_Y, MARGIN_X, MARGIN_Y);
    }

    /// <summary>
    /// Webビューで使うオブジェクトの生成と読み込み（画面外に表示）
    /// </summary>
    private void LoadWebView(string url)
    {
        Destroy(m_webViewObject);

        var go = new GameObject("WebViewObject");

        m_webViewObject = go.AddComponent<WebViewObject>();

        // WebView を初期化します
        m_webViewObject.Init
        (
            cb: msg => Debug.LogFormat("HTML からメッセージを取得しました: {0}", msg),
            err: msg => Debug.LogFormat("エラーが発生しました: {0}", msg),
            started: msg => Debug.LogFormat("ページの読み込みを開始しました: {0}", msg),
            ld: msg => Debug.LogFormat("ページの読み込みが完了しました: {0}", msg),
            enableWKWebView: true
        );

        // WebView のマージンを設定します(画面外へ表示しておく)
        m_webViewObject.SetMargins(MARGIN_X + OUT_MARGIN_AJUST, MARGIN_Y, MARGIN_X - OUT_MARGIN_AJUST, MARGIN_Y);

        m_webViewObject.SetVisibility(true);

        // 指定した URL を読み込みます
        m_webViewObject.LoadURL(url);
    }

    /// <summary>
    /// 閉じるボタン押下時
    /// </summary>
    public void CloseWebView()
    {
        // WebView を非表示状態にします
        //m_webViewObject.SetVisibility(false);

        //LoadWebView(URL);//動画など音が出る場合はオブジェクトは破壊して再読み込み
        // 指定した URL を読み込みます
        // WebView のマージンを設定します
        m_webViewObject.SetMargins(MARGIN_X + OUT_MARGIN_AJUST, MARGIN_Y, MARGIN_X - OUT_MARGIN_AJUST, MARGIN_Y);
        //m_webViewObject.LoadURL(URL);


    }

    /// <summary>
    /// 閉じるボタン押下時
    /// </summary>
    public void CloseSettingButtonOnClick()
    {
        SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
    }


    /// <summary>
    /// 設定ボタン押下時
    /// </summary>
    public void SettingButtonOnClick()
    {
        AudioManager.Instance.PlaySE("OkSe");
        if (this.gameObject.activeSelf)
        {
            SaveData.Save();
            SettingOff();
            //UserDataManager.Instance.DestroyInputObj();
        }
        else
        {
            SettingOn();
        }
    }

    /// <summary>
    /// 設定画面表示
    /// </summary>
    public void SettingOn()
    {
        this.gameObject.SetActive(true);
    }

    /// <summary>
    /// 設定画面非表示
    /// </summary>
    public void SettingOff()
    {
        this.gameObject.SetActive(false);
    }
}
