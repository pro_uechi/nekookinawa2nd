﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

using System.Collections;
using UnityEngine.UI;
using UnityEngine.Analytics;

/// <summary>
/// 課金ウインドウ_ボタン制御
/// </summary>
public class Purchaser : MonoBehaviour, IStoreListener
{
    
    [SerializeField, Header("購入時にもらえるスターの数")]
    private int[] _getStarNums = null;

    [SerializeField, Header("購入時にもらえるスターの数テキスト")]
    private Text[] _getStarNumTexts = null;

    private static IStoreController m_StoreController;              // Purchasing システムの参照
    private static IExtensionProvider m_StoreExtensionProvider;     // 拡張した場合のPurchasing サブシステムの参照

    //public string[] ItemIdList = { "00120", "00600", "01080", "03000", "05000" };


    //private static string kProductIDConsumable = "consumable";           // 消費型製品の汎用ID
    //private static string kProductIDNonConsumable = "nonconsumable";     // 非消費型製品の汎用ID
    //private static string kProductIDSubscription = "subscription";       // 定期購読製品の汎用ID

    // アイテムID. 01-05
    private static string consumable_item_01 = "00120_handpush";
    private static string consumable_item_02 = "00360_handpush";
    private static string consumable_item_03 = "00720_handpush";
    //private static string consumable_item_04 = "03000";
    //private static string consumable_item_05 = "05000";


    // Apple App Store アイテム０１.  // 今回はGoogleと同一キーでSTORE登録したので不要
    //private static string product_apple_consumable_item_01 = "apple.com.test.item01";
    // Apple App Store アイテム０２.
    //private static string product_apple_consumable_item_02 = "apple.com.test.item02";


    // Google Play Store アイテム01-05
    private static string product_google_consumable_item_01 = "00120_handpush";
    private static string product_google_consumable_item_02 = "00360_handpush";
    private static string product_google_consumable_item_03 = "00720_handpush";
    //private static string product_google_consumable_item_03 = "01080";
    //private static string product_google_consumable_item_04 = "03000";
    //private static string product_google_consumable_item_05 = "05000";



    void Awake()
    {
        Debug.Log("Awake()");

        // テキスト描画
        for (int i = 0; i < _getStarNums.Length; i++)
        {
            _getStarNumTexts[i].text = "x" + _getStarNums[i].ToString();
        }

        // ↓変更 消して下に追加
        //// Unity Purchasing 参照が設定されてなければ...
        //if (m_StoreController == null)
        //{
        //    // Purchasing へつなげる初期設定
        //    InitializePurchasing();
        //}

        // Purchasing へつなげる初期設定
        InitializePurchasing();
    }

    public void InitializePurchasing()
    {
        Debug.Log("InitializePurchasing()");


        // ↓変更　消しました
        //// Purchasing へ既に繋がっているんだったら...
        //if (IsInitialized())
        //{
        //    // ここは何もしない
        //    return;
        //}


        var module = StandardPurchasingModule.Instance();

        // フェイクStoreは以下のことをサポートしている： UIなし（いつも成功する）、基本的なUI（購入が成功/失敗）
        // 開発者向けUI（初期化、購入、失敗時の設定）。　これらは FakeStoreUIMode の Enum値と対応している
        //module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser; // フェイクStoreMODEを使う時は有効にする

        var builder = ConfigurationBuilder.Instance(module);


#if UNITY_ANDROID
        // 公開鍵
        builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiCdteGO1bjm0Qd0i5smtEYRSr/gcxWYcQJsa+qH8v9yhvUmKknxeYwXAzQfWvT3jqhRskTBZo/DzyE4kFqToQ7q2tr/due+QDf2jnhp/TmXYJcqi9XDlPUbTuVCZURuM/HMrTEHSxK+VrNCvkmVWtpENsF5d+bYOF9PSjqrNh+bVcmyT//DmBW5GT8iNu1pUY3/w6qx8ZKM5RmaX9XM4pipbdUCd86ka9y0JEFN9aYFsSUusasrVUZjJPAfArJa543KY4wMQU27pEJmZU5IUM87r5wW8nJCuesQRJMwo7jVeyR3NWOl/Yqieky5ggxrN1rKLHC7TRkFUdECPxSidQQIDAQAB");
#endif
        // Add a product to sell / restore by way of its identifier, associating the general identifier with its store-specific identifiers.
        builder.AddProduct(consumable_item_01, ProductType.Consumable, new IDs() {
            { product_google_consumable_item_01, GooglePlay.Name },
            { product_google_consumable_item_01, AppleAppStore.Name },
        });
        builder.AddProduct(consumable_item_02, ProductType.Consumable, new IDs() {
            { product_google_consumable_item_02, GooglePlay.Name },
            { product_google_consumable_item_02, AppleAppStore.Name },
        });
        builder.AddProduct(consumable_item_03, ProductType.Consumable, new IDs() {
            { product_google_consumable_item_03, GooglePlay.Name },
            { product_google_consumable_item_03, AppleAppStore.Name },
        });

        //builder.AddProduct(consumable_item_04, ProductType.Consumable, new IDs() {
        //    { product_google_consumable_item_04, GooglePlay.Name },
        //    { product_google_consumable_item_04, AppleAppStore.Name },
        //});
        //builder.AddProduct(consumable_item_05, ProductType.Consumable, new IDs() {
        //    { product_google_consumable_item_05, GooglePlay.Name },
        //    { product_google_consumable_item_05, AppleAppStore.Name },
        //});

        UnityPurchasing.Initialize(this, builder);
    }


    /// <summary>
    /// 課金処理の初期化状態確認
    /// </summary>
    private bool IsInitialized()
    {
        Debug.Log("IsInitialized()");

        // 二つのPurchasing の参照が設定されていれば、初期設定されていると言える
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }



    /// <summary>
    /// 課金ボタン押下時
    /// </summary>
    public void ChargesButton_OnClick()
    {
        Debug.Log("ChargesButton_OnClick()");

        //this.transform.root.Find("Charges").gameObject.SetActive(true);
    }

    /// <summary>
    /// 利用規約　表示ボタン押下時
    /// </summary>
    public void LegalButton_OnClick()
    {
        Debug.Log("LegalButton_OnClick()");

        AudioManager.Instance.PlaySE("press");
        //#if UNITY_EDITOR_WIN
        Application.OpenURL("http://prolead.co.jp/transaction/handpush_legal.html");  //WebViewに差し替え予定

        //#else 
        //        GameObject SWO = Instantiate(GameObject.Find("Canvas").transform.Find("SettingBoard/Objects/ButtonList/TermsOfServiceButton/WebViewObjectBasic").gameObject);
        //        SWO.SetActive(true);

        //        GameObject.Find("Canvas").transform.Find("SettingBoard/Objects").gameObject.SetActive(false);
        //        GameObject.Find("Canvas").transform.Find("SettingBoard/WebCloseButton").gameObject.SetActive(true);
        //#endif
    }

    /// <summary>
    /// 利用規約表示中 閉じるボタン押下時
    /// </summary>
    public void WebCloseButton_OnClick()
    {
        Debug.Log("WebCloseButton_OnClick()");

        AudioManager.Instance.PlaySE("press");

        // WebViewの停止
        //GameObject.Find("WebViewObject").GetComponent<WebViewObject>().SetVisibility(false);
        //Destroy(GameObject.Find("WebViewObjectBasic(Clone)").gameObject);
        //Destroy(GameObject.Find("WebViewObject").gameObject);
        //GameObject.Find("Canvas").transform.Find("SettingBoard/Objects").gameObject.SetActive(true);
        //GameObject.Find("Canvas").transform.Find("SettingBoard/WebCloseButton").gameObject.SetActive(false);
    }

    /// <summary>
    /// 特定商取引法に基づく表示ボタン押下時
    /// </summary>
    public void TradeLawButton_OnClick()
    {
        Debug.Log("TradeLawButton_OnClick()");

        Application.OpenURL("http://prolead.co.jp/transaction/agreement.html");
    }

    /// <summary>
    /// キャンセルボタン押下時
    /// </summary>
    public void CancelButton_OnClick()
    {
        Debug.Log("CancelButton_OnClick()");

        this.transform.root.Find("Charges").gameObject.SetActive(false);
    }

    /// <summary>
    /// 購入ボタン押下時
    /// </summary>
    public void ConfirmButton_OnClick()
    {
        if(IsInitialized())
        {
            AudioManager.Instance.PlaySE("OkSe");
            GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
            msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("チャージしますか？", delegate { Cofirm(consumable_item_01); Destroy(msgBox); }, null);
        }
    }

    /// <summary>
    /// 購入ボタン押下時
    /// </summary>
    public void ConfirmButton02_OnClick()
    {
        if (IsInitialized())
        {
            AudioManager.Instance.PlaySE("OkSe");
            GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
            msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("チャージしますか？", delegate { Cofirm(consumable_item_02); Destroy(msgBox); }, null);
        }
    }


    /// <summary>
    /// 購入ボタン押下時
    /// </summary>
    public void ConfirmButton03_OnClick()
    {
        if (IsInitialized())
        {
            AudioManager.Instance.PlaySE("OkSe");
            GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
            msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("チャージしますか？", delegate { Cofirm(consumable_item_03); Destroy(msgBox); }, null);
        }
    }

    public void Cofirm(string name)
    {

        // Purchasing が初期化されていれば ...
        if (IsInitialized())
        {
            Debug.Log("if (IsInitialized())");

            // ... 汎用のプロダクト識別子と Purchasing システムの商品コレクションから
            // Product の参照を取得します。
            Product product = m_StoreController.products.WithID(name);

            // このデバイスのストア用のプロダクトが存在し、そのプロダクトが販売可能な状態であれば ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... プロダクトを購入します。ProcessPurchase または OnPurchaseFailed 経由で
                // 非同期的にレスポンスが発生します。
                m_StoreController.InitiatePurchase(product);
            }
            // そうでなければ ...
            else
            {
                // ... product の参照取得の失敗あるいはプロダクトが購入可能でない旨のログを出力します。
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // あるいは ...
        else
        {
            // ... Purchasing がまだ初期化に成功していない旨のログを出力します。更に待機し続けるか 
            // 初期化を再度試みるか検討してください。
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }


    }




    /// <summary>
    /// Called when Unity IAP is ready to make purchases.
    /// </summary>
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        // Purchasing は初期化に成功した。Purchasing 参照を取っておく
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        // 全体のPurchasingシステム。このアプリケーションの製品を構成している。
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        // ストア特有のサブシステム。デバイス特有のストアへのアクセス等ができる。
        m_StoreExtensionProvider = extensions;

        //PhoneDisplay.Instance.Log("初期化成功");

    }

    /// <summary>
    /// Called when Unity IAP encounters an unrecoverable initialization error.
    ///
    /// Note that this will not be called if Internet is unavailable; Unity IAP
    /// will attempt initialization until it becomes available.
    /// </summary>
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // 初期化失敗。
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);

        //PhoneDisplay.Instance.Log("初期化失敗");
    }

    /// <summary>
    /// Called when a purchase completes.
    ///
    /// May be called at any time after OnInitialized().
    /// </summary>
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log("購入応答:ProcessPurchase: OK");

        // Starを購入分加算する 
        if (String.Equals(args.purchasedProduct.definition.id,
            consumable_item_01, StringComparison.Ordinal))
        {
            //GameData.UserData.StarCount += _getStarNums[0];
            StartCoroutine(DBAccessManager.Instance.UpdateUserData());
        }
        else if (String.Equals(args.purchasedProduct.definition.id,
            consumable_item_02, StringComparison.Ordinal))
        {
            //GameData.UserData.StarCount += _getStarNums[1];
            StartCoroutine(DBAccessManager.Instance.UpdateUserData());
        }
        else if (String.Equals(args.purchasedProduct.definition.id,
            consumable_item_03, StringComparison.Ordinal))
        {
            //GameData.UserData.StarCount += _getStarNums[2];
            StartCoroutine(DBAccessManager.Instance.UpdateUserData());
        }
        //else if (String.Equals(args.purchasedProduct.definition.id,
        //    consumable_item_04, StringComparison.Ordinal))
        //{
        //    GameInfo.UserData.StarCount += 400;
        //    GameInfo.UserData.Juwel += 150000;
        //}
        //else if (String.Equals(args.purchasedProduct.definition.id,
        //    consumable_item_05, StringComparison.Ordinal))
        //{
        //    GameInfo.UserData.StarCount += 600;
        //    GameInfo.UserData.Juwel += 300000;

        //}

        StartCoroutine("InsertIAPPlayer", args.purchasedProduct.definition.id);

        //this.transform.root.Find("Charges/LoadingScreen").gameObject.SetActive(false);
        GameObject msgBox = Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        msgBox.GetComponent<MessageBoxManager>().Initialize_OK("スターを購入しました", null);

        return PurchaseProcessingResult.Complete;
    }


    /// <summary>
    /// 購入者DB記録
    /// </summary>
    /// <returns></returns>
    private IEnumerator InsertIAPPlayer(String id)
    {

        // DB更新処理
        yield return StartCoroutine(DBAccessManager.Instance.InsertIAPPlayer(id));
        if (DBAccessManager.Instance.ErrFlg)
        {
            yield break;
        }
    }



    /// <summary>
    /// Called when a purchase fails.
    /// </summary>
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        this.transform.root.Find("Charges/LoadingScreen").gameObject.SetActive(false);
        Debug.Log("購入応答:OnPurchaseFailed !!");
        // 製品購入が成功しなかった。詳しい情報はfailureReasonをチェック。ユーザーに失敗の理由はシェアした方がいい
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        GameObject msgBox = Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        msgBox.GetComponent<MessageBoxManager>().Initialize_OK("キャンセルしました", null);
    }

    /// <summary>
    /// 戻るボタン押下時
    /// </summary>
    public void ReturnButtonOnClick()
    {
        SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
    }
}
