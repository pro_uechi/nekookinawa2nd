﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Clock : MonoBehaviour {
    private GameObject hour;
    private GameObject minite;

	// Use this for initialization
	void Start () {
        GetClockObject();
    }
	
	// Update is called once per frame
	void Update () {
        SetNowClock();
    }

    private void GetClockObject()
    {
        hour = transform.Find("hour").gameObject;
        minite = transform.Find("minite").gameObject;
    }

    private void SetNowClock()
    {
        DateTime dt = DateTime.Now;
        hour.transform.eulerAngles = new Vector3(0,0,(float)dt.Hour/12 * (-360) + (float)dt.Minute / 60 * (-360/12));
        minite.transform.eulerAngles = new Vector3(0, 0, (float)dt.Minute / 60 * (-360));
    }
}
