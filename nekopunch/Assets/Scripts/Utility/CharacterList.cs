﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AutoyaFramework;
using TMPro;

public class CharacterList : MonoBehaviour
{
    TextMeshProUGUI ListText;

    // Use this for initialization
    void Start()
    {
        ListText = transform.Find("Scroll View/Viewport/Content/Text").GetComponent<TextMeshProUGUI>();
        //StartCoroutine(LoadCharacterText());
    }

    private void LoadCharacterList()
    {
        TextAsset text = Resources.Load("CharacterList") as TextAsset;
        ListText.text = text.text;
    }


}
