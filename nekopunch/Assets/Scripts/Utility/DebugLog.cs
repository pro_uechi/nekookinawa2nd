﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; // デバッグ用
using System.Collections;

public class DebugLog : MonoBehaviour
{
    //[SerializeField]
    //private Text m_textUI = null;


    //private void Awake()
    //{
    //    Application.logMessageReceived += OnLogMessage;
    //}

    private void Start()
    {
        DebugLogOff();
    }

    /// <summary>
    /// セーブデータをクリアする
    /// </summary>
    public void ClearSaveData()
    {
        SaveData.Clear();
        //DebugLogOff();
    }

    /// <summary>
    /// タイトルに戻るボタン
    /// </summary>
    public void ReturnToTitle()
    {
        SceneFadeManager.Instance.Load(GameData.Scene_Splash, GameData.FadeSpeed);

        //DebugLogOff();
    }

    /// <summary>
    /// ホームに戻るボタン
    /// </summary>
    public void ReturnToHome()
    {
        SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);

        //DebugLogOff();
    }

    /// <summary>
    /// Debugボタン押下時
    /// </summary>
    public void DebugButtonOnClick()
    {
        if (this.gameObject.activeSelf)
        {
            DebugLogOff();
        }
        else
        {
            DebugLogOn();
        }
    }

    /// <summary>
    /// 全キャラクター取得ボタン押下時
    /// </summary>
    public void GetAllButtonOnClick()
    {
        StartCoroutine(GetAll());
    }

    /// <summary>
    /// ログインボーナスやり直しボタン押下時
    /// </summary>
    public void RedoLoginBonusOnClick()
    {
        GameData.UserData.LoginFlg = 0;
        SceneFadeManager.Instance.Load(GameData.Scene_Home, GameData.FadeSpeed);
    }

    /// <summary>
    /// 無敵ボタン
    /// </summary>
    public void OnClickInvincibleMode()
    {
        GameData.invincibleMode = !GameData.invincibleMode;
        //DebugLogOff();
    }

    public void GetMoney()
    {
        GameData.UserData.Money += 1000;
        // ユーザー情報更新
        StartCoroutine(DBAccessManager.Instance.UpdateUserData());

        //DebugLogOff();
    }

    public void GetStar()
    {
        //GameData.UserData.StarCount += 10;
        //UserDataManager.Instance.UpdateUserData();
    }

    public void DebugRankUp()
    {
        GameData.UserData.UserLevel++;


		// ローカルのユーザー情報を書き換えてセーブ
		SaveData.SetClass("UserData", GameData.UserData);
		//SaveData.SetInt(SaveKey.UserId, GameData.ConvertStringToUserID(id));
		SaveData.Save();


		//GameObject.Find("Canvas_PlayerInfo").GetComponent<PlayerInfoCanvas>().SetRank();
		// ユーザー情報更新
		//StartCoroutine(DBAccessManager.Instance.UpdateUserData());

	}

    public void DebugRankDown()
    {
        GameData.UserData.UserLevel--;
        GameData.UserData.UserLevel = Mathf.Max(-10, GameData.UserData.UserLevel);
		// ローカルのユーザー情報を書き換えてセーブ
		SaveData.SetClass("UserData", GameData.UserData);
		//SaveData.SetInt(SaveKey.UserId, GameData.ConvertStringToUserID(id));
		SaveData.Save();


	}



	/// <summary>
	/// 全キャラ取得状態にする
	/// </summary>
	/// <returns></returns>
	IEnumerator GetAll()
    {
        //// 使用可能なキャラクターの数だけループ
        //yield return StartCoroutine(DBAccessManager.Instance.GetCharacterData_All(0));
        //if (DBAccessManager.Instance.ErrFlg)
        //{
        //    yield break;
        //}

        //// 全キャラ取得
        //foreach (CharacterData data in GameData.CharacterDataList)
        //{
        //    GameData.UserData.MyCharacterDataList.Add(data.CharacterId);
        //}

        //// 所持キャラクター情報更新
        //yield return StartCoroutine(DBAccessManager.Instance.UpdateMyCharacterData());
        //if (DBAccessManager.Instance.ErrFlg)
        //{
        //    yield break;
        //}

        // デバッグ用
        // 現在のScene名を取得する
        Scene loadScene = SceneManager.GetActiveScene();
        //// Sceneの読み直し
        SceneManager.LoadScene(loadScene.name);

		//DebugLogOff();
		yield break;

	}


    /// <summary>
    /// デバッグログオン
    /// </summary>
    private void DebugLogOn()
    {
        this.gameObject.SetActive(true);

        Time.timeScale = 0;
    }

    /// <summary>
    /// デバッグログオフ
    /// </summary>
    private void DebugLogOff()
    {
        this.gameObject.SetActive(false);
        //GameObject.Find("DontDestroy").transform.GetChild(0).gameObject.SetActive(false);

        Time.timeScale = 1;
    }
}