﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogBattleStatus : MonoBehaviour
{
    private Player[] _players = new Player[2];

    [SerializeField, Header("テキスト")]
    public Text[] _text;

    void Start()
    {
        //StartCoroutine(Loop());
        StartCoroutine(Loop());
    }

    IEnumerator Loop()
    {
        yield return null;
        _players[0] = GameData.PlayerObjct.GetComponent<Player>();
        _players[1] = GameData.CpuObjct.GetComponent<Player>();

        while (true)
        {
            DisplayStatus();
            yield return null;
        }
    }

    void DisplayStatus()
    {
        for(int i = 0; i < 2; i++)
        {
            _text[i].text = "HP:" + _players[i].hp.ToString() + "\n"
            + "ATK:" + _players[i].attack.ToString() + "\n"
            + "DEF:" + _players[i].defense.ToString() + "\n"
            + "SPD:" + _players[i].agility.ToString() + "\n";
        }
    }
}
