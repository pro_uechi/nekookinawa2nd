﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initialize_01_Splash : MonoBehaviour
{

	public GameObject playerObject = null;

	private void Start()
    {
        //// プレイヤー非表示
        //GameData.SetPlayerActive(false);

        // ホームプレイヤー非表示
        GameData.SetHomePlayerActive(false);
        SceneFadeManager.Instance.Load(GameData.Scene_DataCheck, GameData.FadeSpeed,false);
		//SceneFadeManager.Instance.Load(GameData.Scene_Title, GameData.FadeSpeed,false);

	}

}
