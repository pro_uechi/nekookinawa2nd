﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Initialize_12_Shop : MonoBehaviour
{
    /// <summary>
    /// アイテム数
    /// </summary>
    private const int MAX_ITEM_VALUE = 4;

    /// <summary>
    /// 各アイテムボタン
    /// </summary>
    [SerializeField, Header("各アイテムボタン")]
    private Button[] _items = new Button[MAX_ITEM_VALUE];

    /// <summary>
    /// 各×ボタン
    /// </summary>
    [SerializeField, Header("各×ボタン")]
    private Button[] _closeButton = new Button[5];

    /// <summary>
    /// 各アイテム値段テキスト
    /// </summary>
    [SerializeField, Header("各アイテム値段テキスト")]
    private Text[] _itemTexts = new Text[MAX_ITEM_VALUE];

    ///// <summary>
    ///// 各アイテムのスターテキスト
    ///// </summary>
    //[SerializeField, Header("各アイテムのスターテキスト")]
    //private Text[] _starTexts = new Text[MAX_ITEM_VALUE];

    /// <summary>
    /// 各アイテムの上昇値テキスト
    /// </summary>
    [SerializeField, Header("各アイテム上昇値テキスト")]
    private Text[] _itemValueTexts = new Text[MAX_ITEM_VALUE];

    /// <summary>
    /// Coin_or_StarMessage
    /// </summary>
    [SerializeField, Header("Coin_or_StarMessage")]
    private GameObject _coin_or_StarMessage;

    /// <summary>
    /// 購入完了メッセージ
    /// </summary>
    [SerializeField, Header("購入完了メッセージ")]
    private GameObject _buyItemMessage;

    /// <summary>
    /// 購入できませんメッセージ
    /// </summary>
    [SerializeField, Header("購入できませんメッセージ")]
    private GameObject _canNotBuyMessage;


    /// <summary>
    /// 初期化処理
    /// </summary>
    private void Awake()
    {
        // ItemManagerの各変数に格納していく
        ItemManager.Instance.ItemPriceTexts = _itemTexts;
        //ItemManager.Instance.ItemBuyStarValueTexts = _starTexts;
        ItemManager.Instance.ItemBuyUpwardValueTexts = _itemValueTexts;
        ItemManager.Instance.Coin_or_StarMessage = _coin_or_StarMessage;
        ItemManager.Instance.BuyItemMessage = _buyItemMessage;
        ItemManager.Instance.CanNotBuyItemMessage = _canNotBuyMessage;

        // ボタンのイベント設定
        _items[0].onClick.AddListener(ItemManager.Instance.OnClickBuyNekoJarashi);
        _items[1].onClick.AddListener(ItemManager.Instance.OnClickBuyBall);
        _items[2].onClick.AddListener(ItemManager.Instance.OnClickBuyMouse);
        _items[3].onClick.AddListener(ItemManager.Instance.OnClickBuyCatFood);
        _closeButton[0].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);
        _closeButton[1].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);
        _closeButton[2].onClick.AddListener(ItemManager.Instance.OnClick_BuyItem_ReturnShop);
        _closeButton[3].onClick.AddListener(ItemManager.Instance.OnClick_BuyItem_ReturnShop);
        _closeButton[4].onClick.AddListener(ItemManager.Instance.OnClickReturnShop);

        // アイテム初期化
        ItemManager.Instance.Initialize();
    }
}
