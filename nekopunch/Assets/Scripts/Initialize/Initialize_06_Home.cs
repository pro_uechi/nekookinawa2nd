﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using AutoyaFramework;

public class Initialize_06_Home : MonoBehaviour
{
    [SerializeField]
    private GameObject _changeNameDialog = null;

    // キャラクター情報
    //[SerializeField]
    //private Text characterName = null;

    private void Awake()
    {
        int startupGame = SaveData.GetInt(SaveKey.IsStartupGameName, 0);

        // 初めてゲームを起動したか？
        if (startupGame == 0)
        {
            /// <summary>
            /// 名前入力画面を表示
            /// </summary>
            _changeNameDialog.SetActive(true);
            GameData.UserData.IsStartupGame = 1;
            SaveData.SetInt(SaveKey.IsStartupGameName, GameData.UserData.IsStartupGame);
            SaveData.Save();




		}

        // BATTLE猫削除、HOME猫生成
        GameObject deleteObject = null;
        deleteObject = GameData.PlayerObjct;
        string CatPrefabIDStr = "Prefabs/cat" + string.Format("{0:00}", SaveData.GetInt(SaveKey.UserCharacter));
        GameObject pObject = Instantiate((GameObject)Resources.Load(CatPrefabIDStr), new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(0, 180, 0));
        DontDestroyOnLoad(pObject);
        GameData.PlayerObjct = pObject;
        GameData.PlayerMeshObject = pObject.transform.Find("mesh").gameObject;
        GameData.PlayerObjct.name = "cat";
        GameData.SetPlayerActive(false);
        Destroy(deleteObject);


        // プレイヤーの大きさを設定
        GameData.SetHomePlayerScale(new Vector3(0.8f, 0.8f, 0.8f));

        /// <summary>
        /// プレイヤーの位置を変更
        /// </summary>
        GameData.SetHomePlayerPosition(GameData.DefalutPos);

        // ホームプレイヤー非表示
        GameData.SetHomePlayerActive(true);

        // PlayerInfoCanvasを表示
        PlayerInfoCanvas.Instance.Enable(true);
        PlayerInfoCanvas.Instance.SetPlayerInfo();

        //BGMを流す
        AudioManager.Instance.PlayBGM("Home");
        // アイテムのアニメーションを流す
        ItemManager.Instance.StartAnimation((int)(DateTime.Now - SaveData.GetDateTime(SaveKey.HomeTime)).TotalSeconds);
    }


    private IEnumerator Start()
    {
		//// プレイヤーの大きさを設定
		//GameData.SetHomePlayerScale(new Vector3(0.8f, 0.8f, 0.8f));

		//// ホームプレイヤー非表示
		//GameData.SetHomePlayerActive(true);

		//// PlayerInfoCanvasを表示
		//PlayerInfoCanvas.Instance.Enable(true);
		//PlayerInfoCanvas.Instance.SetPlayerInfo();

		////BGMを流す
		//AudioManager.Instance.PlayBGM("Home");
		//// アイテムのアニメーションを流す
		//ItemManager.Instance.StartAnimation((int)(DateTime.Now - SaveData.GetDateTime(SaveKey.HomeTime)).TotalSeconds);

		//int startupGame = SaveData.GetInt(SaveKey.IsStartupGameName, 0);

		//PhoneDisplay.Instance.Log("Initialzie_Home");
		//PhoneDisplay.Instance.Log("値は: " + startupGame.ToString());

		//// 初めてゲームを起動したか？
		//if (startupGame == 0)
		//{
		//    PhoneDisplay.Instance.Log("初めてゲームを起動させました。");
		//    _changeNameDialog.SetActive(true);
		//    GameData.UserData.IsStartupGame = 1;
		//    SaveData.SetInt(SaveKey.IsStartupGameName, GameData.UserData.IsStartupGame);
		//    SaveData.Save();
		//}
		//else
		//{
		//    PhoneDisplay.Instance.Log("初めてではありません");
		//}


		int startupGame = SaveData.GetInt(SaveKey.IsStartupGameName, 0);
		if (startupGame == 0)
		{
			// 新規ユーザーをINSERT
			yield return StartCoroutine(DBAccessManager.Instance.InsertNewUser(GameData.UserData.MyCharacterData.CharacterName));
			if (DBAccessManager.Instance.ErrFlg)
			{
				yield break;
			}


		}




		yield break;
	}

    private void Update()
    {
        // maioのポップアップ確認
        MaioManager.Instance.CheckPopUp();
    }

    /// <summary>
    /// 最新バージョンをダウンロードさせるためにストアに誘導
    /// </summary>
    private void GoToStore()
    {
#if UNITY_ANDROID
        Application.OpenURL(GameData.REVIEW_URL_ANDROID);
#else
        Application.OpenURL(GameData.REVIEW_URL_IPHONE);
#endif
    }


}