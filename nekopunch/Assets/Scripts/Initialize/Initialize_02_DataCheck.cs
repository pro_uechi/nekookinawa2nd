﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

using AutoyaFramework;
using AutoyaFramework.AssetBundles;
using System;
using Anima2D;
using TMPro;

/// <summary>
/// このクラスでアセットバンドルのチェック、データベースから各種データを取得などを行う
/// </summary>
public class Initialize_02_DataCheck : MonoBehaviour
{
	public GameObject playerObject = null;

	/// <summary>
	/// アセットバンドルのダウンロード状況(%表示)
	/// </summary>
	//[SerializeField]
    private Text percent;


    IEnumerator Start()
    {

        //// 最初にデータベースからアプリの情報を取得する
        //yield return StartCoroutine(DBAccessManager.Instance.GetAppInfoData());
        //if (DBAccessManager.Instance.ErrFlg)
        //{
        //    yield break;
        //}

        //// メンテナンス中かどうかチェック
        //if (GameData.AppInfoData.maintenance == 0)
        //{
        //    // アプリのバージョンをチェック   
        //    if (AppVersionCheck.CheckLatestVersion())
        //    {
        //        GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        //        msgBox.GetComponent<MessageBoxManager>().Initialize_OK("最新バージョンにしてください。", GoToStore);
        //    }
        //    else
        //    {

        //        StartCoroutine(CheckAssetBundles());
        //    }
        //}
        //else
        //{
        //    GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        //    msgBox.GetComponent<MessageBoxManager>().Initialize_OK(GameData.AppInfoData.maintenanceInfo, null);
        //}
        // データチェック
        yield return StartCoroutine(CheckData());


		// 参照を代入
		//GameObject cat = Instantiate((GameObject)Resources.Load("Prefabs/cat01"));

		//int catid = 1;
		string CatPrefabIDStr = "Prefabs/home_cat" + string.Format("{0:00}", GameData.UserData.CharacterId);
        //GameObject EffectSPCutINPrefab = (GameObject)Instantiate((GameObject)Resources.Load(SPprefabStr));



        //playerObject = Instantiate((GameObject)Resources.Load(CatPrefabIDStr), new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(0, 180, 0));
        //DontDestroyOnLoad(playerObject);
        //GameData.PlayerObjct = playerObject;

        //GameData.PlayerObjct.AddComponent<CPU>();

        //GameData.PlayerObjct.name = "cat";
        //GameData.PlayerMeshObject = playerObject.transform.Find("mesh").gameObject;

        playerObject = Instantiate((GameObject)Resources.Load(CatPrefabIDStr), new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(0, 180, 0));
        DontDestroyOnLoad(playerObject);
        GameData.HomePlayerObject = playerObject;
        GameData.HomePlayerObject.name = "home_cat";
        // ホームプレイヤー非表示
        GameData.SetHomePlayerActive(false);

        //GameData.HomePlayerMeshObject = playerObject.transform.Find("mesh").gameObject;

        /////////////////////////////////////////////////////////////////////////////
        /// 
        /// SAVEDATAを元にユーザー情報を格納
        /// 
        /////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// ユーザがクリアしたレベルを格納
        /// </summary> 
        if (SaveData.GetInt(SaveKey.clearLevel) != 0)
        {
            GameData.UserData.UserLevel = SaveData.GetInt(SaveKey.clearLevel);
        }

        /// <summary>
        /// 猫の種類を格納
        /// </summary> 
        if (SaveData.GetInt(SaveKey.UserCharacter) == 0)
        {
            SaveData.SetInt(SaveKey.UserCharacter, 1);
            SaveData.Save();
        }

        /// <summary>
        /// ユーザーのステータスを格納
        /// </summary>
        float atk = SaveData.GetFloat(SaveKey.ATKData);
        float def = SaveData.GetFloat(SaveKey.DEFData);
        float agy = SaveData.GetFloat(SaveKey.AgilityData);
        float hp = SaveData.GetFloat(SaveKey.HPData);
        int money = SaveData.GetInt(SaveKey.moneyData);
        //int star = SaveData.GetInt(SaveKey.starData);
        string name = SaveData.GetString(SaveKey.UserCatName);

        if (atk != 0) { GameData.UserData.MyCharacterData.ATK = atk; }
        if (def != 0) { GameData.UserData.MyCharacterData.DEF = def; }
        if (agy != 0) { GameData.UserData.MyCharacterData.SPD = agy; }
        if (hp != 0) { GameData.UserData.MyCharacterData.HP = hp; }
        if (money != 0) { GameData.UserData.Money = money; }
        //if (star != 0) { GameData.UserData.StarCount = star; }
        if (name != string.Empty) { GameData.UserData.MyCharacterData.CharacterName = name; }

        /// <summary>
        /// アイテム使用時の上昇量、アイテムの値段を格納
        /// </summary>
        GameData.UserData.itemPriceIndexs = SaveData.GetList<int>(SaveKey.PriceValueData, new List<int>() { 0, 0, 0, 0 });
        GameData.UserData.itemRiseIndexs = SaveData.GetList<int>(SaveKey.RiseValueData, new List<int>() { 0, 0, 0, 0 });

        /// <summary>
        /// アニメーションの名前、アニメーションの時間を格納
        /// </summary>
        GameData.UserData.aniNames = SaveData.GetList<string>(SaveKey.AniNames, new List<string>());
        GameData.UserData.aniTimes = SaveData.GetList<float>(SaveKey.AniTimes, new List<float>());
        GameData.UserData.aniRiseIndexs = SaveData.GetList<int>(SaveKey.AniRiseIndexs, new List<int>());
        GameData.UserData.statusMessageName = SaveData.GetList<string>(SaveKey.StatusMessageName, new List<string>());
        ItemManager.Instance.Initialize(false);
        GoToTitle();
	}

	/// <summary>
	/// 最新バージョンをダウンロードさせるためにストアに誘導
	/// </summary>
	private void GoToStore()
    {
#if UNITY_ANDROID
        Application.OpenURL(GameData.REVIEW_URL_ANDROID);
#else
        Application.OpenURL(GameData.REVIEW_URL_IPHONE);
#endif
    }

    /// <summary>
    /// データチェック
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckData()
    {
        // セーブデータから取得
        GameData.UserData = new UserData();
        GameData.UserData.UserId = SaveData.GetInt(SaveKey.UserId, 0);
        GameData.UserData.CharacterId = SaveData.GetInt(SaveKey.UserCharacter, 1);
        GameData.UserData.examLevel = SaveData.GetInt(SaveKey.examLevel, 0);
        GameData.UserData.examStage = SaveData.GetInt(SaveKey.examStage, 0);
        AudioManager.Instance.SetBGMFlag(SaveData.GetInt(SaveKey.BGMSetting, GameData.SETTING_SOUND_ON));
        AudioManager.Instance.SetSEFlag(SaveData.GetInt(SaveKey.SESetting, GameData.SETTING_SOUND_ON));
        // データベースからユーザーデータを取得してからセーブデータを取得(順序を逆にするとデータベースの情報で上書きされてしまう)
        GameData.UserData.battleCount = SaveData.GetInt(SaveKey.BattleCount, 0);
        //GameData.UserData.examResult = SaveData.GetList(SaveKey.ExamResult, new List<int>());
        GameData.UserData.reviewed = SaveData.GetInt(SaveKey.Reviewed, 0);


        yield return null;
}

/// <summary>
/// アセットバンドルチェック(必要があればサーバーからダウンロードする)
/// </summary>
/// <returns></returns>
IEnumerator CheckAssetBundles()
    {
        // キャッシュクリアして新たにダウンロードさせる(サーバー側で強制的にキャッシュクリアさせる)
        //if ((GameData.UserData.UserId != 0) && GameData.UserData.CleareCache == 0)
        //{
        //    while (!Autoya.AssetBundle_DeleteAllStorageCache())
        //    {
        //        yield return null;
        //    }
        //    Debug.Log("ClearedCache");

        //    yield return StartCoroutine(DBAccessManager.Instance.UpdateCleareCache());
        //    if (DBAccessManager.Instance.ErrFlg)
        //    {
        //        yield break;
        //    }
        //}

        //// 使用可能なキャラクターの数を取得
        //yield return StartCoroutine(DBAccessManager.Instance.GetCharacterData_All(0));
        //if (DBAccessManager.Instance.ErrFlg)
        //{
        //    yield break;
        //}

        //// need to wait finish authentication.
        //while (!Autoya.Auth_IsAuthenticated())
        //{
        //    yield return null;
        //}

        /*
			this is sample of "preload assetBundles feature".
			the word "preload" in this sample means "download assetBundles without use."
			preloaded assetBundles are stored in storage cache. no difference between preloaded and downloaded assetBundles.
			case1:generate preloadList from assetBundleList, then get described assetBundles.
		 */

        // アセットバンドルの更新があるかもしれないので、毎回アセットバンドルリストを削除する(これが正しいやり方なのかは調査中)
        Autoya.AssetBundle_DiscardAssetBundleList(() => { }, (code, reason) => { });

        Autoya.AssetBundle_DownloadAssetBundleListsIfNeed(status => { }, (code, reason, autoyaStatus) => { });

        // wait downloading assetBundleList.
        while (!Autoya.AssetBundle_IsAssetBundleFeatureReady())
        {
            yield return null;
        }

        /*
			let's preload specific assetBundle into device storage.
		*/

        // get assetBundleList.
        var assetBundleLists = Autoya.AssetBundle_AssetBundleLists();

        // create sample preloadList which contains all assetBundle names in assetBundleList.
        var assetBundleNames = assetBundleLists.SelectMany(list => list.assetBundles).Select(abInfo => abInfo.bundleName).ToArray();
        var newPreloadList = new PreloadList("samplePreloadList", assetBundleNames);

        Autoya.AssetBundle_PreloadByList(
            newPreloadList,
            (willLoadBundleNames, proceed, cancel) =>
            {
                // ダウンロードするアセットバンドルのサイズを計算する
                var totalWeight = Autoya.AssetBundle_GetAssetBundlesWeight(willLoadBundleNames);
                Debug.Log("start downloading bundles. total weight:" + totalWeight);

                if (totalWeight > 0)
                {
                    proceed();
                }
                else
                {
                    GoToTitle();
                }
            },
            progress =>
            {
                Debug.Log("progress:" + progress);
                Debug.Log(progress * 100 + "%");
                percent.gameObject.SetActive(true);

                percent.text = (progress * 100).ToString("F1") + "%";
            },
            () =>
            {
                Debug.Log("preloading all listed assetBundles is finished.");
                percent.text = 100 + "%";
                GameObject messageBoxPrefab = (GameObject)Resources.Load("Prefabs/MessageBox");
                GameObject msgBox = Instantiate(messageBoxPrefab);
                msgBox.GetComponent<MessageBoxManager>().Initialize_OK("最新データにしました。", GoToTitle);
            },
            (code, reason, autoyaStatus) =>
            {
                Debug.LogError("preload failed. code:" + code + " reason:" + reason);
            },
            (downloadFailedAssetBundleName, code, reason, autoyaStatus) =>
            {
                Debug.LogError("failed to preload assetBundle:" + downloadFailedAssetBundleName + ". code:" + code + " reason:" + reason);

            },
            10 // 10 parallel download! you can set more than 0.
        );
    }

    private void GoToTitle()
    {
        SceneFadeManager.Instance.Load(GameData.Scene_Title, GameData.FadeSpeed, false);

        // ストレージからメモリーへの展開が遅いのであらかじめ展開しておく
        //foreach (CharacterData element in GameData.CharacterDataList)
        //{
        //    // アセットバンドル内のアセット読み込み
        //    Autoya.AssetBundle_LoadAsset<Sprite>
        //    (
        //        assetName: "Assets/AssetBundleMaterials/CharacterTexture/" + element.UniqId + ".png",
        //        loadSucceeded: (name, sprite) =>
        //        {
        //            Debug.Log(name + "アセットの読み込み 成功");
        //        },
        //        loadFailed: (name, err, reason, status) =>
        //        {
        //            Debug.LogFormat(name + "アセットの読み込み 失敗\n\n{0}\n\n{1}\n\n{2}", err, reason, status);
        //        }
        //    );

        //    // アセットバンドル内のアセット読み込み
        //    Autoya.AssetBundle_LoadAsset<TextAsset>
        //    (
        //        assetName: "Assets/AssetBundleMaterials/Descriptions/" + element.UniqId + "_descriptions.txt",
        //        loadSucceeded: (name, orgText) =>
        //        {
        //            Debug.Log(name + "アセットの読み込み 成功");
        //        },
        //        loadFailed: (name, err, reason, status) =>
        //        {
        //            Debug.LogFormat(name + "アセットの読み込み 失敗\n\n{0}\n\n{1}\n\n{2}", err, reason, status);
        //        }
        //    );
        //}

    }


}
