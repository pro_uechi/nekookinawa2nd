﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Initialize_07_Battle : MonoBehaviour
{


	private void Start()
    {
        // ホーム画面を離れる際に呼ばれる
        ItemManager.Instance.LeaveHomeInitialize();
        // PlayerInfoCanvasをfalse
        PlayerInfoCanvas.Instance.Enable(false);
        // ホームプレイヤー非表示
        GameData.SetHomePlayerActive(false);
        // プレイヤー表示
        GameData.SetPlayerActive(true);
        // プレイヤーの大きさを設定
        GameData.SetPlayerScale(new Vector3(1.8f, 1.8f, 1.8f));

        /// <summary>
        /// バトルBGMを再生する処理(山城)
        /// </summary>
        if (GameData.UserData.examStage >= GameData.numGameToRankConfirm - 1)
        {
            // ボスのBGM
            AudioManager.Instance.PlayBGM("BossBattle");
            // ラストのボスのBGM
            if(GameData.Levelrank == GameData.MaxLevelRank)
            {
                AudioManager.Instance.PlayBGM("LastBossBattle");
            }
        }
        else
        {
            // 最後のレベルなら
            if(GameData.Levelrank == GameData.MaxLevelRank)
            {
                // ボスのBGM
                AudioManager.Instance.PlayBGM("BossBattle");
            }
            else
            {
                // 通常バトルBGM
                AudioManager.Instance.PlayBGM("Battle");
            }
        }

        // ホームの動画広告を再生できるようにする
        UnityAdsManager.showed = false;
        MaioManager.showed = false;
    }
}
