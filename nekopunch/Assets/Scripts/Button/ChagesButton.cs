﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChagesButton : MonoBehaviour
{
    /// <summary>
    /// 課金ボタン押下時
    /// </summary>
    public void OnClickChagesButton()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name != GameData.Scene_Charges)
        {
            CheckEquip(GameData.Scene_Charges);
        }
    }

    /// <summary>
    /// 着せ替えチェック
    /// </summary>
    private void CheckEquip(string sceneName)
    {
        AudioManager.Instance.PlaySE("OkSe");

        //if (EquipManager.Instance.CheckEquipDifference())
        //{
        //    GameObject msgBox = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/MessageBox"));
        //    msgBox.GetComponent<MessageBoxManager>().Initialize_YesNo("この衣装で\nいいですか？", delegate { ConfirmAndGoToScene(sceneName); }, delegate { SceneFadeManager.Instance.Load(sceneName, GameData.FadeSpeed); });
        //}
        //else
        {
            SceneFadeManager.Instance.Load(sceneName, GameData.FadeSpeed);
        }
    }

    /// <summary>
    /// 衣装を確定してから遷移
    /// </summary>
    private void ConfirmAndGoToScene(string sceneName)
    {
        //EquipManager.Instance.ConfirmEquip();
        SceneFadeManager.Instance.Load(sceneName, GameData.FadeSpeed);
    }
}
