﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coin_or_StarMessageButton : MonoBehaviour
{
    #region　変数

    [SerializeField, Header("交流できないメッセージ")]
    private GameObject _canNotBuyMessage;

    [SerializeField, Header("購入完了メッセージ")]
    private GameObject _buyItemMessage;

    [SerializeField, Header("アイテムの価格テキスト")]
    private Text _itemPriceText;

    [SerializeField, Header("アイテムのスター必要数")]
    private Text _itemStarText;

    /// <summary>
    /// お金の価格
    /// </summary>
    private int _price;

    /// <summary>
    /// 必要スター数
    /// </summary>
    private int _starValue;

    #endregion

    private void OnEnable()
    {
        var num = ItemManager.Instance.GetItemPrice();
        _itemPriceText.text = "×\t" + num;
        //_itemStarText.text = "×\t" + num.Item2;
       // Debug.LogError("価格" + num.Item1 + "\t" + "必要スター数" + num.Item2);
    }

    /// <summary>
    /// お金でアイテムを購入する処理
    /// </summary>
    public void OnClickBuyItem_WithMoney()
    {
        // プレイヤーのステータスが最大値か調べる
        if (ItemManager.Instance.Check_MaxPlayerStatus())
        {
            this._canNotBuyMessage.SetActive(true);
            this.gameObject.SetActive(false);
            return;    
        }

        // お金があるか調べる
        if(ItemManager.Instance.CheckBuyItem_WithMoney())
        {
            AudioManager.Instance.PlaySE("OkSe");
            _buyItemMessage.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }

    ///// <summary>
    ///// スターでアイテムを購入する処理
    ///// </summary>
    //public void OnClickBuyItem_WithStar()
    //{
    //    // プレイヤーのステータスが最大値か調べる
    //    if (ItemManager.Instance.Check_MaxPlayerStatus())
    //    {
    //        this._canNotBuyMessage.SetActive(true);
    //        this.gameObject.SetActive(false);
    //        return;
    //    }

    //    // スターがあるか調べる
    //    if (ItemManager.Instance.CheckBuyItem_WithStar())
    //    {
    //        AudioManager.Instance.PlaySE("OkSe");
    //        _buyItemMessage.SetActive(true);
    //        this.gameObject.SetActive(false);
    //    }
    //}

}
