﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TypeButtonController : MonoBehaviour
{
    /// <summary>
    /// タイプボタンオブジェクト
    /// </summary>
    [SerializeField]
    private GameObject typeButtons;

    
    /// <summary>
    /// タイプボタン表示
    /// </summary>
    public void EnableTypeButtons()
    {
        AudioManager.Instance.PlaySE("OkSe");
        typeButtons.SetActive(true);
    }

    /// <summary>
    /// タイプボタン非表示
    /// </summary>
    public void DisableTypeButtons()
    {
        AudioManager.Instance.PlaySE("OkSe");
        typeButtons.SetActive(false);
    }
}
