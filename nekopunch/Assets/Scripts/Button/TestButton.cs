﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestButton : MonoBehaviour {
	public Color color;
	private Texture2D tex = null;

	// Use this for initialization
	void Start () {
		tex = new Texture2D(1, 1, TextureFormat.RGB24, false);
	}
	
	// Update is called once per frame
	void Update () {
		test();
	}

	void HitTexure()
	{
		Vector2 pos = Input.mousePosition;
		tex.ReadPixels(new Rect(pos.x, pos.y, 1, 1), 0, 0);
		color = tex.GetPixel(0, 0);
		Debug.Log(color);
	}

	void test()
	{
		RaycastHit hit;
		if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
			return;

		Renderer renderer = hit.collider.GetComponent<Renderer>();
		Texture2D tex = renderer.material.mainTexture as Texture2D;
		Vector2 pixelUV = hit.textureCoord;
		Debug.Log(hit.textureCoord);
		pixelUV.x *= tex.width;
		pixelUV.y *= tex.height;

		int x = (int)Mathf.Floor(pixelUV.x);
		int y = (int)Mathf.Floor(pixelUV.y);

		Debug.Log("Texture Name : " + hit.transform.gameObject.name + ", u=" + pixelUV.x + ", " +
			"v=" + pixelUV.y + ", w=" + tex.width + ", h=" + tex.height + ", " + tex.GetPixel(x, y));
	}
}
