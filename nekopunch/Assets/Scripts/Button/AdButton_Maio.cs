﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdButton_Maio : MonoBehaviour
{
    /// <summary>
    /// 動画広告ボタン
    /// </summary>
    [SerializeField]
    private GameObject adButton = null;


    void Update()
    {
#if UNITY_IOS
        if (MaioManager.Instance.CanShowAd() && GameData.UserData.LoginFlg == 1)
        {
            if (adButton.activeSelf == false)
            {
                adButton.SetActive(true);
            }
        }
        else
        {
            if (adButton.activeSelf == true)
            {
                adButton.SetActive(false);
            }
        }
#elif UNITY_ANDROID
        if (MaioManager.Instance.CanShowAd() && GameData.UserData.LoginFlg == 1)
        {
            if (adButton.activeSelf == false)
            {
                adButton.SetActive(true);
            }
        }
        else
        {
            if (adButton.activeSelf == true)
            {
                adButton.SetActive(false);
            }
        }
#endif

    }

    /// <summary>
    /// 広告ボタン押下時
    /// </summary>
    public void OnClickAdButton()
    {
        Debug.Log("AdButtonOnClick()");
        MaioManager.Instance.ShowAd(AdType.Home);
    }
}
