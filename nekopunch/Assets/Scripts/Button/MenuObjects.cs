﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuObjects : MonoBehaviour
{
    private GameObject MenuScrollView = null;

    private void Start()
    {
        MenuScrollView = GameObject.Find("Canvas/SafeArea/MenuScrollView");
    }

    /// <summary>
    /// 各メニューボタン押下時
    /// </summary>
    /// <param name="num"></param>
    public void MenuButtonOnclick(int index)
    {
        //MenuScrollView.SetActive(false);

        foreach (Transform child in this.transform)
        {
            child.gameObject.SetActive(false);
        }

        this.transform.GetChild(index).gameObject.SetActive(true);
    }

    /// <summary>
    /// カテゴリーボタンを再表示
    /// </summary>
    public void OnCategoryButton()
    {
        //MenuScrollView.SetActive(true);
        //removeButton.SetActive(false);

        foreach (Transform child in this.transform)
        {
            child.gameObject.SetActive(false);
        }
    }
}
