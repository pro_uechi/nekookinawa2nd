﻿using System;
using UnityEngine;
using System.Collections.Generic;


/// <summary>
/// ユーザー情報
/// </summary>
[Serializable]
public class UserData
{
    /// <summary>
    /// ユーザーID
    /// </summary>
    public int UserId = 0;

    /// <summary>
    /// ユーザーレベル 過去最高クリアレベル
    /// </summary>
    public int UserLevel = 1;
 
    /// <summary>
    /// 使用中キャラクターID
    /// </summary>
    public int CharacterId = 0;

    /// <summary>
    /// 所持ポイント
    /// </summary>
    public int Point = 0;

    /// <summary>
    /// 所持金
    /// </summary>
    public int Money = 0;

    ///// <summary>
    ///// 所持スター
    ///// </summary>
    //public int StarCount = 0;

    /// <summary>
    /// ログインフラグ
    /// </summary>
    public int LoginFlg = 0;

    /// <summary>
    /// キャッシュクリアをするかどうか
    /// </summary>
    public int CleareCache = 0;

    /// <summary>
    /// アイテムデータ
    /// </summary>
    public ItemData MyItemData = new ItemData();

    /// <summary>
    /// 自分の使用キャラ
    /// </summary>
    public CharacterData MyCharacterData = new CharacterData();


    /// <summary>
    /// 所持キャラクターリスト文字列
    /// </summary>
    public string MyCharacterDataListStr = string.Empty;

	/// <summary>
	/// 所持キャラクターリスト
	/// </summary>
	//public List<string> MyCharacterDataList = new List<string>();
	public List<int> MyCharacterDataList = new List<int>();

    /// <summary>
    /// バトル回数
    /// </summary>
    public int battleCount = 0;

	/// <summary>
	/// 現在挑戦中のレベル 0:なし 1-5:途中
	/// </summary>
	public int examLevel = 0;

	/// <summary>
	/// 現在挑戦中のステージ 0:なし 1-5:途中
	/// </summary>
	public int examStage = 0;

	/// <summary>
	/// レビューしたかどうか
	/// </summary>
	public int reviewed = 0;

    /// <summary>
    ///　初めてゲームを起動したか？(0: 初めて起動した 1: 初めてではない)
    /// </summary>
    public int IsStartupGame = 0;

    /// <summary>
    /// 各アイテムの値段を参照するインデックス
    /// </summary>
    public List<int> itemPriceIndexs = new List<int>();

    /// <summary>
    /// 各アイテムの上限値の値を参照するインデックス
    /// </summary>
    public List<int> itemRiseIndexs = new List<int>();

    /// <summary>
    /// 各アニメーション時間
    /// </summary>
    public List<float> aniTimes = new List<float>();

    /// <summary>
    /// 各アニメーションの名前
    /// </summary>
    public List<string> aniNames = new List<string>();

    /// <summary>
    /// 各アニメーションの上昇値のインデックス
    /// </summary>
    public List<int> aniRiseIndexs = new List<int>();

    /// <summary>
    /// ステータスメッセージの名前を取得
    /// </summary>
    public List<string> statusMessageName = new List<string>();

}