﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RewardsItemData
{
    /// <summary>
    /// アイテムID
    /// </summary>
    public int ItemId = 0;

    /// <summary>
    /// アイテムタイプ
    /// </summary>
    public string ItemType = string.Empty;

    /// <summary>
    /// 量
    /// </summary>
    public int Amount = 0;
}
