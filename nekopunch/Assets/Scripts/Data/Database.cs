﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class PictureBookCharacterData
{
    public Sprite img;//画像
	public string name;//名前
	public string description;//説明文章
	public bool getflg = false;//手に入れたかどうか

}

public class Database : SingletonMonoBehaviour<Database>
{

    //エリア判別用変数
    public static int area_num = 1;

    //キャラクターの名前データ
    public static string[] Prefecture_names = new string[GameData.PrefectureVolume];
    //各都道府県のステージに床があるかどうか
    public static bool[] Floor_Flg_Table = new bool[GameData.PrefectureVolume];

    public static int AREA_VOLUME = 8;
    const int VOLUME_HOKKAIDOU = 1;
    const int VOLUME_TOUHOKU = 6;
    const int VOLUME_KANTOU = 7;
    const int VOLUME_TYUBU = 9;
    const int VOLUME_KINKI = 7;
    const int VOLUME_TYUGOKU = 5;
    const int VOLUME_SHIKOKU = 4;
    const int VOLUME_KYUSH = 8;


    public static int PrefectureDataSize = GameData.PrefectureVolume;
    public static int MaxAreaLenth = 8;
    public static int[] AreaLenth = { VOLUME_HOKKAIDOU, VOLUME_TOUHOKU, VOLUME_KANTOU, VOLUME_TYUBU, VOLUME_KINKI, VOLUME_TYUGOKU, VOLUME_SHIKOKU, VOLUME_KYUSH };

    //出身県ごとにデータを分ける(使わないかもしれないので、決まったら消します)
    public List<CharacterData>[] Prefecturedata = new List<CharacterData>[PrefectureDataSize];

    int now_prefecture_num;

    //取得したキャラクターのデータキー
    List<string> GetCharcterDataID = new List<string>();

    static private GameObject obj = null;

    //void Awake()
    //{
    //    //if (obj != null)
    //    //{
    //    //	Destroy(this.gameObject);
    //    //	return;
    //    //}
    //    //obj = this.gameObject;
    //    //DontDestroyOnLoad(this.gameObject);
    //}

    // Use this for initialization
    void Start()
    {
        for(int i = 0; i < Floor_Flg_Table.Length; i++)
        {
            Floor_Flg_Table[i] = true;
        }
        //// 使用可能なキャラクターの数だけループ
        //yield return StartCoroutine(DBAccessManager.Instance.GetCharacterData_All(0));
        //if (DBAccessManager.Instance.ErrFlg)
        //{
        //    yield break;
        //}

        //// 全キャラ取得状態
        //foreach (var element in GameData.CharacterDataList)
        //{
        //    GetCharacter(element.UniqId);
        //}

        Load();
        //LoadPrefectureNameByText();
    }

    // Updata is called once per frame
    void Update()
    {
        
    }




    /// <summary>
    /// キャラクターのゲット処理
    /// </summary>
    /// /// <param name="id">キャラのID</param>
    public void GetCharacter(string id)
    {
        if (GetCharcterDataID.Contains(id))
        {
            return;
        }
        GetCharcterDataID.Add(id);
        //Save();
    }


    /// <summary>
    /// キャラクター獲得情報をセーブする
    /// </summary>
    public void Save()
    {
        //データ変更
        SaveData.SetList(SaveKey.GetCharData, GetCharcterDataID);
        SaveData.Save();
    }

    //ロードする
    public void Load()
    {
        GetCharcterDataID = SaveData.GetList<string>(SaveKey.GetCharData, new List<string>());
        SaveData.Save();
    }
}