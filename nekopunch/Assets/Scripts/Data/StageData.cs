﻿using System;
using UnityEngine;

/// <summary>
/// ユーザー情報
/// </summary>
[Serializable]
public class StageData
{
	/// <summary>
	/// レベルNo.
	/// </summary>
	public int Level;

	/// <summary>
	/// ステージ名
	/// </summary>
	public string name;

	/// <summary>
	/// 
	/// </summary>
	//public int answer; 
}