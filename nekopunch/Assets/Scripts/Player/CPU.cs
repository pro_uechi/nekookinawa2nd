﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CPUState
{
    Normal,
    Cheat
}

public class CPU : Player
{
    /// <summary>
    /// 行動時間
    /// </summary>
    private float actionTime = 0.7f;

    /// <summary>
    /// 累計時間
    /// </summary>
    private float totalTime = 0;

    /// <summary>
    /// CPUが行動を起こす割合(0～1)
    /// </summary>
    private float actionRate = 0;

    /// <summary>
    /// チート行動を起こす割合
    /// </summary>
    private float cheatRate = 0;

    /// <summary>
    /// CPUの状態
    /// </summary>
    private CPUState cpuState;

    /// <summary>
    /// フレーム数
    /// </summary>
    private int frame = 0;

    /// <summary>
    /// 待ちフレーム
    /// </summary>
    private int waitFrame = 0;
    
    /// <summary>
    /// 攻撃のパーセント
    /// </summary>
    private int atkPercent = 0;

    private void Awake()
    {
        // アニメーターを代入
        animator = GetComponent<Animator>();
        //　初期化　
        hp = GameData.BattleData.CpuHpTable[GameData.CpuTableNo];
    }

    private void Start()
    {
        // バトルタイプ設定
        SetBattleType(BattleType.Cpu);

        // 最初の待ちフレームを設定
        waitFrame = Random.Range(20, 61);
    }

    private void Update()
    {
        totalTime += Time.deltaTime;

        if (totalTime >= actionTime)
        {
            totalTime = 0;
            float rate = Random.Range(0.0f, 1.0f);
            if (cheatRate > rate)
            {
                cpuState = CPUState.Cheat;
            }
            else
            {
                cpuState = CPUState.Normal;
            }
        }

        if (action)
        {
            // 状態で行動分岐
            switch (cpuState)
            {
                case CPUState.Normal:
                    Normal();
                    break;

                case CPUState.Cheat:
                    Cheat();
					break;
            }
        }
    }

    /// <summary>
    /// ノーマル状態
    /// </summary>
    private void Normal()
    {
        totalTime += Time.deltaTime;

        if (totalTime >= actionTime)
        {
            totalTime = 0;

            float rate = Random.Range(0.0f, 1.0f);
            if (actionRate > rate)
            {
                int actionType = Random.Range(0, 100);

                // actionタイプが設定した値以下なら攻撃
				if (actionType <= atkPercent)
                {
                    // 攻撃
                    Attack();
                    
                }
                else
                {
                    // 回避
                    Avoidance();
                }
            }
        }
    }

    /// <summary>
    /// チート
    /// </summary>
    private void Cheat()
    {

        if (GameData.PlayerObjct.GetComponent<HUM>().animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            Avoidance();
        }

        if (GameData.PlayerObjct.GetComponent<HUM>().animator.GetCurrentAnimatorStateInfo(0).IsName("Normal"))
        {
            if (frame == waitFrame)
            {
				if (specialgauge >= 100)
				{
					Special();
				}
				else
				{
					Attack();
				}
			
                frame = 0;
                waitFrame = Random.Range(20, 61);
            }
            ++frame;
        }
        else
        {
            frame = 0;
        }
    }

    /// <summary>
    /// CPUの行動確率を設定する
    /// </summary>
    public void SetActionRate()
    {

		// レベルに応じて行動割合を決定
		actionRate = GameData.BattleData.actionProbabilityTable[GameData.CpuTableNo] * 0.01f;

		// レベルに応じてチート割合を決定
		cheatRate = GameData.BattleData.cheatProbabilityTable[GameData.CpuTableNo] * 0.01f;

        // 攻撃の割合を設定
        atkPercent = GameData.BattleData.actionAtkPercentTable[GameData.CpuTableNo];

	}

    /// <summary>
    /// 初期化処理
    /// </summary>
    public override void InitializeStatus()
    {
        hp = GameData.BattleData.CpuHpTable[GameData.CpuTableNo];
        attack = GameData.BattleData.CpuAtkTable[GameData.CpuTableNo];
        defense = GameData.BattleData.CpuDefTable[GameData.CpuTableNo];
        agility = GameData.BattleData.CpuSpsTable[GameData.CpuTableNo];
    }
}
