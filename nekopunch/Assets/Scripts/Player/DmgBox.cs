﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmgBox : MonoBehaviour
{
    public GameObject parent = null;


    /// <summary>
    /// 当たり判定(やられ判定)
    /// </summary>
    /// <param name="dmgbox"></param>
    private void OnTriggerEnter2D(Collider2D col)
    {
        //parent.GetComponent<Player>().specialgauge -= 10;
		parent.GetComponent<Player>().OnTriggerEnter2D_DmgBox(col);
    }
}
